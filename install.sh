#!/bin/bash

set -euo pipefail  # https://gist.github.com/maxisam/e39efe89455d26b75999418bf60cf56c

pacman_install() { sudo pacman -S --needed --noconfirm "$@"; }
AUR_helper_install() { paru -S --needed --noconfirm "$@"; }

if [ "$1" != "no-gui" ]; then
    echo "Installing for GUI setup"
else
    echo "Installing for terminal setup"
fi

printf "Continue? [y/N] "
read -r response
response=$(echo "$response" | tr '[:upper:]' '[:lower:]')  # tolower
if [ "$response" = "y"  ] || [ "$response" = "yes" ]; then
    echo "Executing script..."
else
    echo "Exiting script..."
    exit 0
fi

sudo pacman -Syy

pacman_install base-devel

# Random stuff
# udiskie - automount USB disks
echo "####################################################################################"
echo "Random stuff"
echo "####################################################################################"
if [ "$1" != "no-gui" ]; then
    pacman_install git rofi openssh xclip acpilight sxiv  \
                   snapper alsa-utils \
                   reflector \
                   iw man nodejs npm python-pipx udiskie shellcheck \
                   cronie keepassxc
else
    pacman_install git openssh xclip \
                   snapper \
                   reflector \
                   iw man nodejs npm python-pipx udiskie shellcheck \
                   cronie
fi

sudo systemctl enable sshd.service
sudo systemctl start sshd.service
sudo systemctl enable cronie.service
sudo systemctl start cronie.service

# Gitlab fix for `client_global_hostkeys_private_confirm: server gave bad signature for RSA key 0`
# https://stackoverflow.com/a/66876321/14246508
# https://www.reddit.com/r/archlinux/comments/lyazre/openssh_update_causes_problems/
mkdir -p ~/.ssh
FILE=~/.ssh/config
{
    echo "Host gitlab.com"
    echo "  UpdateHostKeys no"
} >> $FILE

sudo systemctl enable reflector.timer
sudo systemctl start reflector.timer

# AUR helper
echo "####################################################################################"
echo "AUR helper"
echo "####################################################################################"
git clone https://aur.archlinux.org/paru.git ~/paru
# Use a ( subshell ) to avoid having to cd back.
(
    cd ~/paru || exit
    makepkg -si
)

# Audio stuff
# pavucontrol - PulseAudio Volume Control
# playerctl - programmatic API for controlling media players - https://wiki.archlinux.org/title/MPRIS
echo "####################################################################################"
echo "Audio stuff"
echo "####################################################################################"
if [ "$1" != "no-gui" ]; then
    pacman_install pavucontrol
    # Bluetooth headphones
    pacman_install bluez bluez-utils pulseaudio-bluetooth playerctl
    sudo systemctl enable bluetooth
    # Add headphones
    # - https://www.youtube.com/watch?v=OZKjSkk_BfY
    # - https://wiki.archlinux.org/title/Bluetooth_headset#Headset_via_Bluez5/PulseAudio
    # `bluetoothctl`
    # `power on`
    # `scan on`
    # Enter paring mode on headphones
    # Copy MAC address
    # `pair MAC_ADDR`
    # `trust MAC_ADDR`
    # `connect MAC_ADDR`
    # `scan off`

    # Setting up auto connection
    echo "### Automatically switch to newly-connected devices" | sudo tee --append /etc/pulse/default.pa
    echo "load-module module-switch-on-connect" | sudo tee --append /etc/pulse/default.pa
fi

# Notification daemon - dunst
# qtile stuff - pyxdg, dbus-next, iwlib
# libnotify - Library for sending desktop notifications (notify-send)
echo "####################################################################################"
echo "Window manager"
echo "####################################################################################"
if [ "$1" != "no-gui" ]; then
    pacman_install xorg-server dunst libnotify picom

    ## qtile
    #echo "qitle"
    #pacman_install qtile
    #pacman_install python-pyxdg python-dbus-next python-iwlib python-xlib
    #sudo cp qtile.desktop /usr/share/xsessions/

    # LeftWM
    echo "LeftWM"
    AUR_helper_install leftwm wmctrl
    pacman_install feh polybar inotify-tools pamixer blueman
fi

# Terminal emulator - Alacritty
echo "####################################################################################"
echo "Terminal emulator"
echo "####################################################################################"
pacman_install alacritty

# Display manager (login manager)
# https://wiki.archlinux.org/title/Display_manager
echo "####################################################################################"
echo "Display manager"
echo "####################################################################################"
if [ "$1" != "no-gui" ]; then
    pacman_install xorg-server

    # SDDM
    pacman_install sddm
    sudo systemctl enable sddm.service
fi

# Terminal stuff
# Neovim - (terminal) editor
# Delta    - A viewer for git and diff output
# Starship - customizable prompt for any shell
# HSTR     - Navigate and search your command history (replacement for `CTRL+R`)
# lazygit  - A simple terminal UI for git commands
# bat      - A cat(1) clone with wings
# ncdu - is a disk utility (NCurses Disk Usage)
echo "####################################################################################"
echo "Terminal suff"
echo "####################################################################################"
AUR_helper_install starship hstr lazygit
pacman_install htop rsync tmux bash-completion fzf ncdu bat
pacman_install neovim git-delta
pacman_intall wget unzip ripgrep fd tree-sitter-cli # Needed by Neovim stuff

# Set git editor to be nvim
git config --global core.editor nvim

# Install python neovim module
# pacman_install python-neovim

# Browser
echo "####################################################################################"
echo "Browser"
echo "####################################################################################"
if [ "$1" != "no-gui" ]; then
    pacman_install chromium
    AUR_helper_install brave-bin
    AUR_helper_install librewolf-bin
    # needed for H.264 decoding (not bundled with librewolf, also enable `media.gmp-gmpopenh264.enabled` in `about:config`
    pacman_install openh264
    # Librewolf and keepassxc-browser integration
    # https://github.com/keepassxreboot/keepassxc/issues/6907#issuecomment-1595803754
fi

# Monitor setup
# autorandr - https://github.com/phillipberndt/autorandr
#           - Save your current display configuration and setup with: `autorandr --save SETUP_NAME`
#           - To make some display primary run
#             - `xrandr --listmonitors`
#             - `xrandr --output MONITOR --primary`
#             - `autorandr --save SETUP_NAME`
echo "####################################################################################"
echo "Monitor setup"
echo "####################################################################################"
if [ "$1" != "no-gui" ]; then
    pacman_install autorandr
fi

# GUI file manager
echo "####################################################################################"
echo "GUI file manager"
echo "####################################################################################"
if [ "$1" != "no-gui" ]; then
    # https://wiki.archlinux.org/title/thunar
    pacman_install thunar gvfs thunar-archive-plugin thunar-media-tags-plugin thunar-volman tumbler ffmpegthumbnailer
fi

# Android phone connection
# mtpfs - enable MTP support
# jmtpfs - `mtpfs` sometimes is not enough to provide MTP
# gvfs-mtp - integrating the MTP protocol with your File Manager
#            (file managers that use GVFS (GNOME Files) and not KIO (KDE's Dolphin))
# Reference: https://linuxhint.com/connect-android-arch-linux/
echo "####################################################################################"
echo "GUI file manager - more stuff"
echo "####################################################################################"
pacman_install mtpfs
AUR_helper_install jmtpfs
if [ "$1" != "no-gui" ]; then
    pacman_install gvfs-mtp
fi

# GUI code editor
echo "####################################################################################"
echo "GUI code editor"
echo "####################################################################################"
doom_emacs_install() {
    pacman_install emacs ripgrep fd xpdf hunspell-en_gb hunspell-en_us
    AUR_helper_install hunspell-sl

    git clone https://github.com/hlissner/doom-emacs ~/.emacs.d
    ~/.emacs.d/bin/doom install
    rm -rf ~/.doom.d
}
if [ "$1" != "no-gui" ]; then
    AUR_helper_install vscodium-bin
    # doom_emacs_install
fi

# Media programs
echo "####################################################################################"
echo "Media programs"
echo "####################################################################################"
if [ "$1" != "no-gui" ]; then
    pacman_install vlc obs-studio peek flameshot
fi

# Other (python-gobject is needed by redshift-gtk)
echo "####################################################################################"
echo "Other GUI apps"
echo "####################################################################################"
if [ "$1" != "no-gui" ]; then
    pacman_install redshift python-gobject qbittorrent discord \
                   qalculate-gtk libreoffice-fresh gimp okular calibre gparted \
                   screenkey
    AUR_helper_install nextcloud-client
fi

# Compress and archive apps
# https://wiki.archlinux.org/index.php/Archiving_and_compression
# handle files - https://wiki.archlinux.org/index.php/P7zip
# GUI only - https://archlinux.org/packages/community/x86_64/xarchiver/
echo "####################################################################################"
echo "Compress and archive apps"
echo "####################################################################################"
pacman_install p7zip xarchiver

#echo "####################################################################################"
#echo "Battery saving stuff"
#echo "####################################################################################"
#if [ "$1" != "no-gui" ]; then
#    #AUR_helper_install auto-cpufreq
#    #sudo systemctl enable auto-cpufreq.service
#    #sudo systemctl start auto-cpufreq.service
#fi

# Adwaita: adwaita-qt, gnome-theme-standard
# Breeze: breeze, breeze-gtk
# GUI programs for theme changing: lxappearance (GTK), qt5ct (QT)
# xsettings: X11 xsettings daemon
#            (every gtk app will listen to it, if you want to live reload to a different
#            theme edit `~/.config/xsettingsd/xsettingsd.conf` and force reload of
#            the config with `killall -s HUP xsettingsd`
#            https://www.reddit.com/r/archlinux/comments/r34p5i/comment/hmb6sxk/?utm_source=share&utm_medium=web2x&context=3
echo "####################################################################################"
echo "Theme stuff"
echo "####################################################################################"
if [ "$1" != "no-gui" ]; then
    pacman_install xsettingsd lxappearance qt5ct breeze breeze-gtk gnome-themes-standard
    #AUR_helper_install adwaita-qt5-git
    echo "QT_QPA_PLATFORMTHEME=qt5ct" | sudo tee --append /etc/environment
fi

echo "####################################################################################"
echo "External monitor brightness control"
echo "####################################################################################"
# https://wiki.archlinux.org/title/backlight#External_monitorshttps://wiki.archlinux.org/index.php/backlight#External_monitors
# https://clinta.github.io/external-monitor-brightness/
# https://www.ddcutil.com/kernel_module/
#if [ "$1" != "no-gui" ]; then
#    # Minimal setup to work with - `ddcutil setvcp 10 - 20` and `ddcutil setvcp 10 + 20`
#    pacman_install ddcutil
#    echo "i2c_dev" | sudo tee --append /etc/modules-load.d/modules.conf
#    sudo usermod -G i2c -a bzgec
#    sudo modprobe i2c-dev
#
#    #pacman_install brightnessctl
#    #AUR_helper_install ddcci-driver-linux-dkms
#
#    ## Enable debug messages
#    ## https://gitlab.com/ddcci-driver-linux/ddcci-driver-linux/-/issues/2#note_18474254
#    #echo "options ddcci dyndbg" | sudo tee --append /etc/modprobe.d/ddcci.conf
#    #echo "options ddcci-backlight dyndbg" | sudo tee --append /etc/modprobe.d/ddcci.conf
#
#    #sudo cp ddcci@.service /etc/systemd/system/
#    #sudo cp 99-ddcci.rules /etc/udev/rules.d/
#    #sudo udevadm control --reload-rules && sudo udevadm trigger
#    #sudo modprobe i2c-dev
#fi


# Enable colors for pacman
#sudo sed -i 's/#Color/Color/' /etc/pacman.conf/

# https://wiki.archlinux.org/index.php/Xorg/Keyboard_configuration
# https://wiki.archlinux.org/title/Activating_numlock_on_bootup
# systemd-numlockontty - Enable numlock with systemd
echo "####################################################################################"
echo "Keyboard stuff"
echo "####################################################################################"
AUR_helper_install systemd-numlockontty
sudo cp keymap/xkb/symbols/si_colemak /usr/share/X11/xkb/symbols/

echo "####################################################################################"
echo "Fix Windows and Linux showing different times"
echo "####################################################################################"
sudo timedatectl set-ntp true
sudo hwclock --systohc
if [ "$1" != "no-gui" ]; then
    timedatectl set-local-rtc 1 --adjust-system-clock
fi

echo "####################################################################################"
echo "Touchpad: change scrolling direction, enable tapping..."
echo "####################################################################################"
if [ "$1" != "no-gui" ]; then
    sudo cp 40-libinput.conf /etc/X11/xorg.conf.d/
fi

echo "####################################################################################"
echo "Setting default applications"
echo "####################################################################################"
# https://www.reddit.com/r/linuxquestions/comments/5z3n81/default_applications_in_arch_linux/dev24vo?utm_source=share&utm_medium=web2x&context=3
# Location: /usr/share/applications/
# Get default for specific file: xdg-mime query filetype myImage.jpg
# Set default: xdg-mime default sxiv.desktop image/jpg
# Get default: xdg-mime query default application/pdf
if [ "$1" != "no-gui" ]; then
    xdg-mime default sxiv.desktop image/jpeg
    xdg-mime default sxiv.desktop image/jpg
    xdg-mime default sxiv.desktop image/png
    xdg-mime default sxiv.desktop image/svg+xml
    xdg-mime default sxiv.desktop image/gif
    xdg-mime default sxiv.desktop image/bmp
    xdg-mime default pcmanfm.desktop inode/directory
    xdg-mime default okularApplication_pdf.desktop application/pdf
    xdg-mime default nvim.desktop text/plain
    xdg-mime default librewolf.desktop text/html
    xdg-mime default librewolf.desktop x-scheme-handler/http
    xdg-mime default librewolf.desktop x-scheme-handler/https
    xdg-mime default librewolf.desktop application/json
fi

echo "####################################################################################"
echo "Set max usage in journald.conf"
echo "####################################################################################"
sudo sed -i '/#SystemMaxUse=/c\SystemMaxUse=200M' /etc/systemd/journald.conf

# Copy wallpapers collection
echo "####################################################################################"
echo "Copy wallpapers collection"
echo "####################################################################################"
if [ "$1" != "no-gui" ]; then
    cp -r ./wallpapers-collection ~/wallpapers-collection
fi

# Setup snapper (btrfs)
echo "####################################################################################"
echo "Setup snapper"
echo "####################################################################################"
if [ "$1" != "no-gui" ]; then
    ./scripts/snapper_setup.py
fi

# Copy configuration files
echo "####################################################################################"
echo "Copy configuration files"
echo "####################################################################################"
bash ./scripts/setup_config.sh

# This must be called after `ssh-agent.service` is copied to `~/.config/systemd/user/`
# This must be called after `playerctld.service` is copied to `~/.config/systemd/user/`
echo "####################################################################################"
echo "Enable custom systemd services"
echo "####################################################################################"
systemctl enable --user ssh-agent
systemctl enable --user playerctld

bash ./scripts/setup_firewall.sh
