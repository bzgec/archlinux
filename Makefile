ALL_FILES_SH := $(shell find -name "*.sh")

# Backup existing configuration from PC to this repository
backup_config_to_repo:
	bash ./scripts/backupExistingConfig.sh


# Check all .sh files with `shellcheck`
check:
	@$(foreach file, \
	  $(ALL_FILES_SH), \
	  echo "#################################################"; \
	  echo "Checking $(file)";  \
	  echo "#################################################"; \
	  shellcheck $(file) || exit 1; \
	)

