#!/bin/sh

set -euo pipefail  # https://gist.github.com/maxisam/e39efe89455d26b75999418bf60cf56c

pacman_install() { sudo pacman -S --needed --noconfirm "$@"; }

mkdir -p ~/.config
cp -a ./dotfiles/. ~/

echo "####################################################################################"
echo "Used by scripts"
echo "####################################################################################"
pacman_install python-typer python-rich

(
    cd ~/.config/scripts/mic || exit
    make
)

