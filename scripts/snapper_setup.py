#!/usr/bin/env python

import os
import getpass

partitions = [
    {"config_name": "notes", "path": "$HOME/notes"},
    {"config_name": "projects", "path": "$HOME/projects"},
]

"""
sudo umount /home/bzgec/projects/.snapshots
sudo rm -r /home/bzgec/projects/.snapshots
sudo snapper -c projects create-config /home/bzgec/projects
sudo btrfs subvolume delete /home/bzgec/projects/.snapshots
sudo mkdir /home/bzgec/projects/.snapshots/
sudo chmod 750 /home/bzgec/projects/.snapshots
sudo chmod a+rx /home/bzgec/projects/.snapshots
sudo chown :"$user" /home/bzgec/projects/.snapshots

sudo systemctl enable --now snapper-timeline.timer
sudo systemctl enable --now snapper-cleanup.timer
"""


def exec_cmd(cmd: str, print_cmd=True):
    if print_cmd is True:
        print(cmd)

    os.system(cmd)


if __name__ == "__main__":
    USERNAME = getpass.getuser()
    HOME_PATH = f"/home/{USERNAME}"
    print(USERNAME)
    print(HOME_PATH)

    for partition in partitions:
        # Replace "$HOME" for full path
        if "$HOME" in partition["path"]:
            partition["path"] = partition["path"].replace("$HOME", HOME_PATH)

        print(
            f"Generating Snapper configuration named '{partition['config_name']}' with the path '{partition['path']}'"
        )
        print("############################################################")

        exec_cmd(f"sudo umount {partition['path']}/.snapshots")
        exec_cmd(f"sudo rm -r {partition['path']}/.snapshots")
        exec_cmd(
            f"sudo snapper -c '{partition['config_name']}' create-config '{partition['path']}'"
        )
        exec_cmd(f"sudo btrfs subvolume delete {partition['path']}/.snapshots")
        exec_cmd(f"sudo mkdir {partition['path']}/.snapshots")
        exec_cmd(f"sudo chmod 750 {partition['path']}/.snapshots")
        exec_cmd(f"sudo chmod a+rx {partition['path']}/.snapshots")
        #exec_cmd(f"sudo chown :{USERNAME} {partition['path']}/.snapshots")
        exec_cmd(f"sudo sed -i '/ALLOW_USERS=/c\ALLOW_USERS=\"{USERNAME}\"' '/etc/snapper/configs/{partition['config_name']}'")
        exec_cmd(f"sudo sed -i '/ALLOW_GROUPS=/c\ALLOW_GROUPS=\"{USERNAME}\"' '/etc/snapper/configs/{partition['config_name']}'")
        exec_cmd(f"sudo sed -i '/SYNC_ACL=/c\SYNC_ACL=\"yes\"' '/etc/snapper/configs/{partition['config_name']}'")

    # This lines below are not needed if you have cron daemon - https://wiki.archlinux.org/title/Snapper
    # exec_cmd("sudo systemctl enable --now snapper-timeline.timer")
    # exec_cmd("sudo systemctl enable --now snapper-cleanup.timer")

    print("!!!!!")
    print("Now you can edit configs in `/etc/snapper/configs/`")
    print("!!!!!")

