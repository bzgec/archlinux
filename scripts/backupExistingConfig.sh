#!/bin/bash
# Copy important configuration from current PC to dotfiles/

set -euo pipefail  # https://gist.github.com/maxisam/e39efe89455d26b75999418bf60cf56c

configToBackup=(
    '.bashrc'
    '.bash_profile'
    '.xprofile'
    '.tmux.conf'
    '.fonts'
    '.config/alacritty/alacritty.toml'
    '.config/autorandr/'
    '.config/bat/'
    '.config/doom/'
    '.config/dunst/'
    '.config/gtk-3.0/settings.ini'
    '.config/keepassxc/'
    '.config/nvim/'
    '.config/picom/'
    '.config/qt5ct/qt5ct.conf'
    '.config/qtile/'
    '.config/redshift/'
    '.config/rofi/'
    '.config/starship.toml'
    '.config/systemd/user/'
    '.config/xsettingsd/xsettingsd.conf'
    '.config/leftwm'
    '.config/scripts'
    '.gitconfig'
    '.gtkrc-2.0'
)

scriptPath="$(cd -- "$(dirname "$0")" || exit 1 >/dev/null 2>&1 ; pwd -P )"
parentFolder="$(dirname "$scriptPath")"
dotfilesFolder="$parentFolder/dotfiles"

(
    cd "$HOME"
    for toCopy in "${configToBackup[@]}"
    do
       echo '~'"/$toCopy"
       rsync -a --delete --relative "$toCopy" "$dotfilesFolder" --exclude=.git --exclude=__pycache__ --exclude=.mypy_cache
       #rsync --exclude=.git --exclude=__pycache__ --exclude=.mypy_cache -r "$HOME/$toCopy" "$dotfilesFolder/$toCopy"
    done
)
