#!/bin/bash

# Steps to Clean Arch Linux - https://averagelinuxuser.com/clean-arch-linux/
# Docker and BTRFS - https://gist.github.com/hopeseekr/cd2058e71d01deca5bae9f4e5a555440
# System maintenance - https://wiki.archlinux.org/title/System_maintenance
# Manage your dotfiles - https://www.chezmoi.io/quick-start/
# Disk Usage - https://dev.yorhel.nl/ncdu

set -euo pipefail  # https://gist.github.com/maxisam/e39efe89455d26b75999418bf60cf56c

###############################################################################
# Package managers
###############################################################################
echo "Remove unused packages (orphans)"
sudo pacman -Rns "$(pacman -Qtdq)"

echo "Clean pacman and paru cache"
sudo pacman -Scc
paru -Scc

echo "Update pacman mirrorlist"
sudo reflector --latest 20 --protocol https --sort rate --save /etc/pacman.d/mirrorlist

###############################################################################
# Logs
###############################################################################
# Delete Log files older than 10 days
#find /var/log/journal/ -type f -mtime +10
# Limit logs size (keep only the latest), not permanent solution
#sudo journalctl --vacuum-size=100M
# Limit time, not permanent solution
#sudo journalctl --vacuum-time=1weeks

# THIS IS THE BEST OPTION, it is permanent, effective after reboot
# Set max usage in journald.conf
#sudo sed -i '/#SystemMaxUse=/c\SystemMaxUse=100M' /etc/systemd/journald.conf

###############################################################################
# Snapper
###############################################################################
# List snapper images
snapper -c home list
snapper -c root list
