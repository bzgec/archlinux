===================
Arch Linux Setup
===================

:info: My Arch Linux Setup

:Authors:
    Bzgec

.. role:: bash(code)
   :language: bash

.. contents:: Table of Contents
   :depth: 2


Usefull info
============

Map ``caps lock`` to ``backspace``
----------------------------------
* :bash:`setxkbmap -option caps:backspace`
* automatically ran on startup

Second keyboard doesn't have the same layout
--------------------------------------------
* get the device number: :bash:`libinput list-devices` (the number after ``/dev/input/event``)
* set new keyboard layout: :bash:`setxkbmap -device [device_number] si`

Get key events/codes
--------------------
* :bash:`xev` (xorg-xev package)

Touchpad reverse scrolling direction
------------------------------------
* Edit `/usr/share/X11/xorg.conf.d/40-libinput.conf` and add
  ``Option "NaturalScrolling" "true"`` to touchpad setting:
  ::

    /usr/share/X11/xorg.conf.d/40-libinput.conf
    ---------------------------------------------------
    Section "InputClass"
            Identifier "libinput touchpad catchall"
            MatchIsTouchpad "on"
            MatchDevicePath "/dev/input/event*"
            Driver "libinput"
            Option "NaturalScrolling" "true"
            Option "Tapping" "on"
            Option "DisableWhileTyping" "on"
    EndSection

Brightness control with xbacklight
----------------------------------
If you don't have permission check `arch wiki <https://wiki.archlinux.org/index.php/Backlight>`__
(know that user must be in ``video`` group (:bash:`usermod -aG video <user>`))

If you get ``No outputs have backlight property`` error when running it :bash:`xbacklight`.

Add to ``/usr/share/X11/xorg.conf.d/50-backlight.conf``:
::

  /usr/share/X11/xorg.conf.d/50-backlight.conf
  -----------------------------------------------
  ```
  Section "Device"
      Identifier  "Intel Graphics"
      Driver      "intel"
      Option      "Backlight"  "intel_backlight"
  EndSection

`This <https://askzorin.com/t/error-while-setting-up-custom-brightness-keys-with-xbacklight/105/3>`__ link helped me.
I had ``/sys/class/backlight/intel_backlight``.

Can't control sound with amixer
-------------------------------
My error was: ``amixer: Unable to find simple control 'Master',0``

1. Print available cards: :bash:`cat /proc/asound/cards`
   ::

     /proc/asound/cards
     ---------------------------------------------------------------
      0 [NVidia         ]: HDA-Intel - HDA NVidia
                           HDA NVidia at 0xd1000000 irq 96
      1 [Generic        ]: HDA-Intel - HD-Audio Generic
                           HD-Audio Generic at 0xd15c0000 irq 97

   We can see that our card is ``1`` and not ``0``

2. Edit/create ``/etc/asound.conf`` and change ``defaults.ctl.card`` and ``defaults.pcm.card``:
   (previously it was ``0`` now set it to ``1``)
   ::

     /etc/asound.conf
     ----------------------------------
     defaults.ctl.card 1
     defaults.pcm.card 1

Resources
~~~~~~~~~
* `askubuntu.com <https://askubuntu.com/a/673334>`__
* `bss.asrchlinux.org - Alsa audio won't work <https://bbs.archlinux.org/viewtopic.php?id=200806>`__

Mount disk at boot
------------------
We are doing the right way - with :bash:`fstab`

1. Get UUID of the disk: :bash:`ls -al /dev/disk/by-uuid/`
2. Get file system format of the partition: :bash:`file -sL /dev/sd*`
3. Configure :bash:`fstab` file: :bash:`sudo vim /etc/fstab`
   ::

     /etc/fstab
     ----------------------------------------------------------------------------------------------------
     # Static information about the filesystems.
     # See fstab(5) for details.

     # <file system> <dir> <type> <options> <dump> <pass>
     # Windows - /dev/nvme0n1p3
     UUID=C25684FB5684F189  /home/USER/win10  ntfs  rw,nosuid,nodev,user_id=0,group_id=0,allow_other,blksize=4096  0 0

     # Home - /dev/nvme0n1p7
     UUID=20cce99b-5a1f-4e98-9a1e-351f31df1c4c  /home  btrfs  rw,noatime,compress=lzo,ssd,space_cache=v2,subvolid=257  0 0

     # Swap - /dev/nvme0n1p6
     UUID=eb530593-0307-4678-a1f2-9c9065574950  none  swap  defaults  0 0

     # Internal HDD - /dev/sda1
     UUID=7D524647407BEC2A  /home/USER/SlimBoi  ntfs  defaults  0 0

New btrfs disk
--------------

1. :bash:`sudo mkfs.btrfs -f -L projects /dev/nvme1n1p3`
2. :bash:`mount /dev/nvme1n1p3 /mnt`
3. :bash:`btrfs su cr /mnt/@`
4. :bash:`btrfs su cr /mnt/@snapshots`
5. :bash:`umount /mnt`
6. :bash:`mount -o noatime,compress=lzo,space_cache=v2,subvol=@ /dev/nvme1n1p3 /mnt`
7. :bash:`mkdir -p /mnt/.snapshots`
8. :bash:`mount -o noatime,compress=lzo,space_cache=v2,subvol=@snapshots /dev/nvme1n1p3 /mnt/.snapshots`

9. :bash:`ls -al /dev/disk/by-uuid/`

   .. code::

      85914935-3e4b-4339-a8b4-c7a316a0ce28 -> ../../nvme1n1p3

10. :bash:`sudo btrfs subvolume list /mnt`

    .. code::

       ID 256 gen 11 top level 5 path @
       ID 257 gen 8 top level 5 path @snapshots

11. :bash:`nvim /etc/fstab`

    .. code::

       # projects - /dev/nvme1n1p3
       UUID=85914935-3e4b-4339-a8b4-c7a316a0ce28  /home/bzgec/projects  btrfs  rw,noatime,compress=lzo,ssd,space_cache=v2,subvolid=256  0 0
       UUID=85914935-3e4b-4339-a8b4-c7a316a0ce28  /home/bzgec/projects/.snapshots  btrfs  rw,noatime,compress=lzo,ssd,space_cache=v2,subvolid=257  0 0

12. Reboot and check if new btrfs is OK:

    - sudo btrfs filesystem usage /home/bzgec/projects
    - sudo btrfs filesystem usage /
    - sudo btrfs subvolume list /home/bzgec/projects
    - sudo btrfs subvolume list /

      .. code::

         ID 256 gen 11 top level 5 path @
         ID 257 gen 8 top level 5 path @snapshots

13. Snapper setup

    1. :bash:`sudo umount /home/bzgec/projects/.snapshots`
    2. :bash:`sudo rm -rf /home/bzgec/projects/.snapshots`
    3. :bash:`sudo snapper -c home create-config /projects`
    4. :bash:`sudo btrfs subvolume delete /home/bzgec/projects/.snapshots`
    5. :bash:`sudo mkdir /home/bzgec/projects/.snapshots/`
    6. :bash:`sudo mount -a`
    7. :bash:`sudo chmod 750 /home/bzgec/projects/.snapshots`
    8. :bash:`sudo chmod a+rx /home/bzgec/projects/.snapshots`
    9. :bash:`sudo chown bzgec /home/bzgec/projects/.snapshots`
    10. Edit snapper config: :bash:`nvim /etc/snapper/configs/projects`

NTFS partition permissions
--------------------------

In order to set permissions to NTFS partition mount as (note the ``permissions`` option):

::

  UUID=235639C5768BC70B   /home/bzgec/storage     ntfs        rw,auto,user,permissions,blksize=4096   0 0

Resources
~~~~~~~~~
- `random post <https://confluence.jaytaala.com/display/TKB/Mount+drive+in+linux+and+set+auto-mount+at+boot>`__
- `get partition (fs) format - unix.stackexchange.com <https://unix.stackexchange.com/a/60783>`__

Disable microphone
------------------
* `ALSA - Advanced Linux Sound Architecture <https://wiki.archlinux.org/index.php/Advanced_Linux_Sound_Architecture>`__
* Toggle microphone: :bash:`amixer set Capture toggle`

Fix missing GRUB
----------------
1. Boot into live arch USB
2. mount your linux partition:

   - btrfs: :bash:`mount -o noatime,compress=lzo,space_cache=v2,subvol=@ /dev/nvme0n1p7 /mnt`
   - other (not tested): :bash:`mount -t auto /dev/nvme0n1p7 /mnt`

3. mount EFI partition: :bash:`mount /dev/nvme0n1p5 /mnt/boot`
   You can check which one is EFI with :bash:`fdisk -l`
4. change root into system: :bash:`arch-chroot /mnt`
5. recreate kernel image (don't know if necessary): :bash:`mkinitcpio -p linux`
6. install grub:

   - EFI: :bash:`grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB`

7. Generate grub configuration: :bash:`grub-mkconfig -o /boot/grub/grub.cfg (you should see OS images)`

   .. note::
      If you have problem with detecting OS:

      1. edit ``/etc/default/grub``
      2. add or uncomment ``GRUB_DISABLE_OS_PROBER=false``
      3. save that file then run ``grub-mkconfig -o /boot/grub/grub.cfg``

      `reference <https://forum.endeavouros.com/t/warning-os-prober-will-not-be-executed-to-detect-other-bootable-partitions-systems-on-them-will-not-be-added-to-the-grub-boot-configuration-check-grub-disable-os-prober-documentation-entry/13998/2>`__

8. :bash:`exit`
9. :bash:`umount -R /mnt`
10. :bash:`reboot`

USB Stick not detected
----------------------
* If your USB is not detected you probably updated your system,
  now you need to restart your PC and it should work
  ::

    general if your kernel updates, all old modules are removed which in practice means no new hardware is being detected

* `Reference <https://bbs.archlinux.org/viewtopic.php?id=234335>`__

Replace old `btrfs` disk
------------------------

1. Start replacement: :bash:`sudo btrfs replace start /dev/nvme0n1p7 /dev/nvme1n1p4 /`

   * ``/dev/nvme0n1p7`` is old partition
   * ``/dev/nvme1n1p4`` is new partition
   * ``/`` is mount point of `btrfs`

2. Monitor progress: :bash:`sudo btrfs replace status /`
3. In case new partition is bigger you need to resize the filesystem to take advantage
   of the new size: :bash:`sudo btrfs filesystem resize 1:max /`
4. Check if everything is ok: :bash:`sudo btrfs filesystem usage /`

* Reference: `Btrfs/Replacing a disk <https://wiki.tnonline.net/w/Btrfs/Replacing_a_disk>`__

Reinstall all packages
----------------------

* :bash:`pacman -Qq | while read -r package; do pacman -S "$package" --overwrite "*" --noconfirm; done`
* Reference: https://unix.stackexchange.com/a/659761

Manually delete `snapper`  `btrfs` snapshot
-------------------------------------------

.. DANGER::

   Leave `/.snapshots/1/snapshot` alone

1. :bash:`btrfs property set /.snapshots/<version>/snapshot ro false`
2. :bash:`rm -r /.snapshots/<version>`

* Reference: https://www.suse.com/support/kb/doc/?id=000019594

Before formatting
=================

1. Before formatting run :bash:`make backup_config_to_repo` to backup existing configuration
   to the repository.

Development
===========

Dependencies
------------
* ``shellcheck``

Before commiting
----------------
Run :bash:`make check` to check for best coding standards.

Random
======

* `reflector <https://wiki.archlinux.org/index.php/reflector>`__ - retrieve the latest mirror list from
  the Arch Linux Mirror Status page, filter the most up-to-date mirrors, sort them by speed and
  overwrite the file ``/etc/pacman.d/mirrorlist``
* `snapper <https://wiki.archlinux.org/index.php/snapper>`__
* `qbittorrent <https://archlinux.org/packages/community/x86_64/qbittorrent/>`__ - BitTorrent client
* `Redshift <https://github.com/jonls/redshift>`__ - `config <config/redshift.toml>`__
  (`wiki.archlinux.org <https://wiki.archlinux.org/index.php/redshift>`__)
* `Element <https://element.io/>`__ - messaging app
* `Discord <https://discord.com/>`__ (`wiki.archlinux.org <https://wiki.archlinux.org/index.php/Discord>`__)
* `backup to external hard drive <scripts/backupScript/README.md>`__
* DistroTube `video <https://www.youtube.com/watch?v=FX26s8INUYo>`__ explaining what you typically need
* Copy configuration files to ``~/.config/`` folder: :bash:`bash scripts/copyConfig.sh`
* `nvim <scripts/nvim/README.md>`__
* `paru <https://github.com/Morganamilo/paru>`__ - AUR helper and pacman wrapper
* `Alacritty <https://github.com/alacritty/alacritty>`__ - terminal emulator
* `Starship <https://starship.rs>`__ - shell prompt - `config <config/starship.toml>`__
* Window Managers:

  * Bare bone:

    * `dwm <https://dwm.suckless.org/>`__ - C

  * Not so bare bone:

    * `leftwm <https://github.com/leftwm/leftwm>`__ - Rust
    * `qtile <http://www.qtile.org/>`__ - Python
    * `awesomewm <https://awesomewm.org/>`__ - Lua

* Display manager (login manager):

  * `SDDM <https://wiki.archlinux.org/title/SDDM>`__
  * `LightDM <https://wiki.archlinux.org/index.php/LightDM>`__

* Application launcher

  * `rofi <https://github.com/davatorium/rofi>`__ - A window switcher, Application launcher and dmenu replacement
    (`wiki.archlinux.org <https://wiki.archlinux.org/index.php/Rofi>`__)

* Firewall

  * `Uncomplicated Firewall (ufw) <https://wiki.archlinux.org/title/Uncomplicated_Firewall>`__

* `dmenu <https://wiki.archlinux.org/index.php/dmenu>`__ - dynamic menu for X

* *linux-lts* long term support kernel
* `Multi-head, multi-screen, multi-display or multi-monitor <https://wiki.archlinux.org/index.php/Multihead>`__
* Keyboard configuration:

  * `Xorg <https://wiki.archlinux.org/index.php/Xorg/Keyboard_configuration>`__ - only for Desktop
    Environment
  * `Linux console <https://wiki.archlinux.org/index.php/Linux_console/Keyboard_configuration>`__ -
    only for virtual console

* Connect to WiFI - `Network Manager <https://wiki.archlinux.org/index.php/NetworkManager>`__

  * ``nmcli`` - command line interface
  * ``nmtui`` - ncurses base interface

pacman
------
* ``--needed``: Do not reinstall the targets that are already up-to-date.
* ``-S``: Install packages.
* ``-R``: Remove a package (keep dependencies).
* ``-Rs``: Remove a package and remove dependencies which are not required by any other installed
  package. If it fails on a group try ``-Rsu``.
* ``-Qtd``: check for packages that were installed as a dependency but now, no other packages depend
  on them
* Generally avoid using:

  * ``--overwrite``: pacman will bypass file conflict checks
  * ``-Sy``: partial upgrades (use ``-Syu``)
  * ``-d``: skips dependency checks during package removal
  *  `AUR helpers <https://wiki.archlinux.org/index.php/AUR_helpers>`__ which automate installation
     of AUR packages (yay, paru)

Removing unused packages (orphans)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
For recursively removing orphans and their configuration files: :bash:`pacman -Qtdq | pacman -Rns -`

References
~~~~~~~~~~
* `wiki.archlinux.org - pacman <https://wiki.archlinux.org/index.php/pacman>`__
* `wiki.archlinux.org - System maintenance <https://wiki.archlinux.org/index.php/System_maintenance>`__

CPU
---
* `Ryzen - wiki.archlinux.org <https://wiki.archlinux.org/index.php/Ryzen>`__
* `microcode - wiki.wrchlinux.org <https://wiki.archlinux.org/index.php/microcode>`__

GPU
---
* `NVIDIA <https://wiki.archlinux.org/index.php/NVIDIA>`__
* `optimus manager <https://github.com/Askannz/optimus-manager>`__
  (`wiki.archlinux.org <https://wiki.archlinux.org/index.php/NVIDIA_Optimus>`__)

Media programs
--------------
* vlc
* obs-studio
* `peek <https://github.com/phw/peek>`__ - animated GIF recorder
* `flameshot <https://github.com/flameshot-org/flameshot>`__ - screenshot software

TODO
====
* ``[ ]`` Fix bad DPI
* ``[ ]`` Display GPU temperature (do you really need this? - could cause problems
  when GPU should be sleeping but is not because of temperature readings...)
* ``[ ]`` Widget for volume control, and play buttons

* ``[x]`` Panel/wibox visible if window is in full screen mode (VLC)
* ``[x]`` First screen OFF after `x` min and then suspend after `y` min
* ``[x]`` closing the lid, pressing power button
* ``[x]`` Microphone ON/OFF
* ``[x]`` Dropdown menus in applications are transparent
