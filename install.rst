=======
Install
=======
:info: My Arch Linux Install notes

:Authors:
    Bzgec

.. role:: bash(code)
   :language: bash

- Dualboot with Windows 10 - `video <https://www.youtube.com/watch?v=L1B1O0R1IHA>`__
- Btrfs with snapper - `video <https://www.youtube.com/watch?v=Xynotc9BKe8>`__
- Installation guide - `wiki.archlinux.org <https://wiki.archlinux.org/index.php/installation_guide>`__

Note that for installation you need internet connection

Set up the correct keyboard
===========================
:bash:`ls /usr/share/kbd/keymaps/**/*.map.gz | less`

Select keyboard
===============
:bash:`loadkeys slovene.map.gz`

Verify the boot mode
====================
:bash:`cat /sys/firmware/efi/fw_platform_size`

If the command returns ``64``, then system is booted in UEFI mode and has a 64-bit x64 UEFI. If the command returns ``32``, then system is booted in UEFI mode and has a 32-bit IA32 UEFI; while this is supported, it will limit the boot loader choice to systemd-boot. If the file does not exist, the system may be booted in BIOS (or CSM) mode.

Check if wireless card is not blocked
=====================================
:bash:`rfkill list`

Connect to WiFi
===============
1. :bash:`iwctl`
2. First, if you do not know your wireless device name, list all Wi-Fi devices: :bash:`device list`
3. Then, to scan for networks: :bash:`station [device] scan`
4. You can then list all available networks: :bash:`station [device] get-networks`
5. Finally, to connect to a network: :bash:`iwctl --passphrase [passphrase] station [device] connect [SSID]`
6. Done

Plug in ethernet cable and check if you have connection
=======================================================
:bash:`ping archlinux.org`

Update the system clock
=======================
:bash:`timedatectl set-ntp true`

You can verify it with: :bash:`timedatectl status`

Update the mirrors
==================
:bash:`sudo pacman -Syy`

Partition the disks
===================
- `wiki.archlinux.org <https://wiki.archlinux.org/index.php/Partitioning>`__

Verify if you have BIOS or UEFI
-------------------------------
:bash:`cat /sys/firmware/efi/fw_platform_size` (if no error than you have UEFI)

Identify devices
----------------
:bash:`lsblk` or :bash:`fdisk -l`

Open selected disk
------------------
:bash:`fdisk /dev/sda` or :bash:`fdisk /dev/nvme0n1`

Partition
---------

BIOS with MBR
~~~~~~~~~~~~~
1. Select MBR/DOS partition table: ``o`` (skip if Windows 10 is already installed)
2. List free unpartitioned space: ``F``
3. Create new partition: ``n``
4. Set partition type to primary: ``p``
5. Set partition number to 1: ``1``
6. Set first sector (where to start first sector) - leave default (just hit enter)
7. Set swap size (equals to RAM size): ``+2G``
8. If some error occurs about type just ignore it
9. Set rest of the disk size: ``n``, ``p``, ``2``, default, default (all remaining memory)
10. Change swap partition type: ``t``, ``1``, ``82`` (this should be checked with ``L``
11. Enable boot from linux partition: ``a``, ``2``
12. Write table to disk: ``w``

.. note::
   If you have dual boot with Windows then you are going to have problem
   with max number of primary partitions.
   You can delete Windows partition with size around 550MB.
   Now you should have two empty partitions (one for SWAP and one for system).

UEFI with GPT
~~~~~~~~~~~~~
1. ``g`` to select GPT partition table (skip if Windows 10 is already installed)
2. Create efi partition: ``n``, ``1``, default, ``+550M``  TODO: is EFI partition really necessary if Windows 10 is already installed?
3. Create swap partition: ``n``, ``2``, default, ``+16G`` (size of RAM)
4. Create Linux partition: ``n``, ``3``, default, default
5. Change partitions type:

   1. ``t``, ``1``, ``1`` (check with ``l`` - EFI)
   2. ``t``, ``2``, ``19`` (check with ``l`` - swap)
   3. Already Linux file system

6. Write table to disk: ``w``

Make file systems (format)
==========================
1. EFI: :bash:`mkfs.fat -F32 /dev/sda1` (only if using UEFI)
2. swap:
   1. Create swap: :bash:`mkswap /dev/sda2`
   2. Turn on swap: :bash:`swapon /dev/sda2`

3. Big partition: :bash:`mkfs.ext4 /dev/sda3`
+

If you want to create ntfs: :bash:`mkfs.ntfs /dev/sda4`
(``-f`` - perform quick (fast) format. This will skip both zeroing of the volume and
bad sector checking.``

Mount partitions
================

1. Main partition: :bash:`mount /dev/sda3 /mnt`
2. Boot partition: :bash:`mount --mkdir /dev/sda1 /mnt/boot`

Btrfs only:
===========
1. Create *root* subvolume: :bash:`btrfs su cr /mnt/@`
2. Create *home* subvolume: :bash:`btrfs su cr /mnt/@home`
3. Create *snapshots* subvolume: :bash:`btrfs su cr /mnt/@snapshots`
4. Create *var_log* subvolume: :bash:`btrfs su cr /mnt/@var_log`
5. Unmount ``/mnt``: :bash:`umount /mnt`
6. Mount *root* subvolume with btrfs: :bash:`mount -o noatime,compress=lzo,space_cache=v2,subvol=@ /dev/nvme0n1p7 /mnt`
7. Make directories: :bash:`mkdir -p /mnt/{boot,home,.snapshots,var_log}`
8. Mount other subvmolumes:
   1. :bash:`mount -o noatime,compress=lzo,space_cache=v2,subvol=@home /dev/nvme0n1p7 /mnt/home`
   2. :bash:`mount -o noatime,compress=lzo,space_cache=v2,subvol=@snapshots /dev/nvme0n1p7 /mnt/.snapshots`
   3. :bash:`mount -o noatime,compress=lzo,space_cache=v2,subvol=@var_log /dev/nvme0n1p7 /mnt/var_log`
   4. :bash:`mount /dev/nvme0n1p5 /mnt/boot` (EFI ONLY)
9. Mount Windows 10: :bash:`mkdir /mnt/win10 && mount /dev/nvme0n1p3 /mnt/win10`

Install essential packages (base system)
========================================

Get best mirrors
----------------
:bash:`sudo pacman -S reflector && reflector --latest 10 --sort rate --save /etc/pacman.d/mirrorlist`

Update the mirrorns
------------------
:bash:`sudo pacman -Syy`

Install essential packages
--------------------------
:bash:`pacstrap -K /mnt base linux linux-firmware vim`

Generate file system table
==========================
:bash:`genfstab -U /mnt >> /mnt/etc/fstab`

We can check if everything is ok with: :bash:`cat /mnt/etc/fstab`

Change root into the new system
===============================
:bash:`arch-chroot /mnt`

Set timezone
============
:bash:`ln -sf /usr/share/zoneinfo/Europe/Ljubljana /etc/localtime` (you can help
yourself with :bash:`ls /usr/share/zoneinfo`)

Set hardware clock
==================
:bash:`hwclock --systohc`

Select locale
=============
:bash:`vim /etc/locale.gen`, uncomment ``en_US.UTF-8 UTF-8`` and ``sl_SI.UTF-8 UTF-8``

Generate the locales
====================
:bash:`locale-gen`

Set the ``LANG`` variable
=========================
:bash:`vim /etc/locale.conf`, enter ``LANG=en_US.UTF-8`` (or simpler just:
:bash:`echo "LANG=en_US.UTF-8" >> /etc/locale.conf`)

Make Arch Linux remember keyboard layout
========================================
:bash:`vim /etc/vconsole.conf`, enter ``KEYMAP=slovene``
(or simpler: :bash:`echo "KEYMAP=slovene" >> /etc/vconsole.conf`)

Create hostname
===============

A hostname is a unique name created to identify a machine on a network.

:bash:`vim /etc/hostname`, input just the hostname: ``arch`` (or simpler:
:bash:`echo "arch" >> /etc/hostname`)

Add hostname to hosts
=====================
:bash:`vim /etc/hosts`, add this lines:
::

  /etc/hosts
  --------------------------------------
  127.0.0.1    localhost
  ::1          localhost

Initramfs
=========
Creating a new ``initramfs`` is usually not required, because ``mkinitcpio`` was run on installation of the kernel package with ``pacstrap``.
:bash:`mkinitcpio -P`

Create root password
====================
:bash:`passwd`, enter root password

Install other packages
======================
:bash:`pacman -S --noconfirm sudo memtest86+ networkmanager network-manager-applet wireless_tools wpa_supplicant efibootmgr ntfs-3g dosfstools os-prober mtools base-devel linux-lts`

Note that ``os-prober`` is needed only for multiboot setups.

Enable previously installed packages
------------------------------------
1. :bash:`systemctl enable NetworkManager`
2. :bash:`systemctl enable wpa_supplicant`

Set ``EDITOR`` environment variable
===================================
:bash:`echo "export EDITOR=vim" >> /etc/profile`

Btrfs only
==========
1. Add ``btrfs`` into modules in :bash:`vim /etc/mkinitcpio.conf` file (``MODULES=(btrfs)``
2. Recreate kernel image with btrfs module included: :bash:`mkinitcpio -p linux`


Boot loader
===========
Choose and install a Linux-capable boot loader. If you have an Intel or AMD CPU, enable microcode updates in addition.

Install
-------
- AMD: :bash:`pacman -S grub amd-ucode`
- Intel: :bash:`pacman -S grub intel-ucode`

Init grub
---------
- BIOS: :bash:`grub-install --target=i386-pc /dev/sda`
- EFI: :bash:`grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB`

Modify grub config
------------------

- Disable grub submenu (all available kernel options are shown on the main screen)
- Configure grub to recall the latest kernel entry you booted and use it as the boot entry

:bash:`vim /etc/default/grub` and change:

::

  /etc/default/grub
  --------------------------------------
  GRUB_DISABLE_SUBMENU=y
  GRUB_DEFAULT=saved
  GRUB_SAVEDEFAULT=true

To make the changes effective you need to re--generate the configuration file.

Generate grub configuration
---------------------------
:bash:`grub-mkconfig -o /boot/grub/grub.cfg` (you should see Linux and Windows 10 images)

If you have problem with detecting OS:
--------------------------------------
1. edit ``/etc/default/grub``
2. add or uncomment ``GRUB_DISABLE_OS_PROBER=false``
3. save that file then run :bash:`grub-mkconfig -o /boot/grub/grub.cfg`

`reference <https://forum.endeavouros.com/t/warning-os-prober-will-not-be-executed-to-detect-other-bootable-partitions-systems-on-them-will-not-be-added-to-the-grub-boot-configuration-check-grub-disable-os-prober-documentation-entry/13998/2>`__

Not detecting Windows OS
------------------------
For Windows installed in UEFI mode, make sure the EFI system partition containing the Windows Boot Manager (bootmgfw.efi) is mounted. Run os-prober as root to detect and generate an entry for it.

For Windows installed in BIOS mode, mount the Windows system partition (its file system label should be System Reserved or SYSTEM). Run os-prober as root to detect and generate an entry for it.

`<https://wiki.archlinux.org/title/GRUB>`__

Create user
===========
:bash:`useradd -mG wheel bzgec`, `passwd bzgec`, enter password

Add user to groups
==================
:bash:`usermod -aG wheel,audio,video,optical,storage bzgec`

Add user to use sudo privileges
===============================
:bash:`EDITOR=vim visudo`, uncomment line ``%wheel ALL=(ALL) ALL``

Enable hibernation
==================
1. Add kernel parameter (add ``resume=`` parameter):

   1. Edit ``/etc/default/grub`` and append your kernel options between the quotes in the
      ``GRUB_CMDLINE_LINUX_DEFAULT`` line:
      ::

        /etc/default/grub
        -------------------------------------------------------------------------
        GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet resume=/dev/nvme0n1p6"

   2. And then automatically re-generate the ``grub.cfg`` file with:
       :bash:`grub-mkconfig -o /boot/grub/grub.cfg`

2. Configure the ``initramfs`` (add ``resume`` hook). Whether by label or by UUID, the swap
   partition is referred to with a udev device node, so the ``resume`` hook must go after the
   ``udev`` hook. This example was made starting from the default hook configuration:
   ::

     /etc/mkinitcpio.conf
     --------------------------------------------------------------------------
     HOOKS=(base udev autodetect modconf block filesystems keyboard resume)

- `Kernel parameters <https://wiki.archlinux.org/index.php/kernel_parameters>`__
- `Power management - suspend and hibernate <https://wiki.archlinux.org/index.php/Power_management/Suspend_and_hibernate#Hibernation>`__

Change options for closing the laptop lid, power button press
=============================================================
- `Power management <https://wiki.archlinux.org/index.php/Power_management#ACPI_events>`__.

Edit ``/etc/systemd/logind.conf``:
::

  /etc/systemd/logind.conf
  --------------------------------------------------
  HandlePowerKey=hibernate
  HandleLidSwitch=suspend-then-hibernate
  HandleLidSwitchExternalPower=suspend-then-hibernate
  IdleAction=suspend-then-hibernate
  IdleActionSec=10min

You can also change delay between suspend and hibernate, edit ``/etc/systemd/sleep.conf``:
::

  /etc/systemd/sleep.conf
  --------------------------------------------------
  HibernateDelaySec=120min

``exit``
========

``umount -R /mnt``
==================

``reboot``
==========

Connect to WiFi
===============
1. List available WiFis: :bash:`nmcli device wifi list`
2. Connect: :bash:`nmcli device wifi connect [SSID] password [PASSWORD]`
3. List all the connected networks: :bash:`nmcli connection show`
4. Check status of network devices: :bash:`nmcli device`
5. Disconnect network: :bash:`nmcli device disconnect [DEVICE]`
6. Re-connect with a network: :bash:`nmcli connection show`
7. Disable WiFi: :bash:`nmcli radio wifi off`

You could also use ``nmtui`` - ncurses based interface

Clone my automated Arch linux setup
======================================
:bash:`git clone https://github.com/bzgec/archlinux.git && cd archlinux && git submodule update --init --recursive`

Setup for servers
=================

Set static IP
-------------
Open :bash:`nmtui`:
1. Edit connection for selected WiFi
2. Set IPv4 configuration to ``<Manual>``
3. *Address*: Put your static IP address - ``192.168.64.3/24``
4. *Gateway*: Router gateway - ``192.168.64.1``
5. *DNS servers*: ... - ``8.8.8.8``

Restart WiFi service...

Check for IP with: :bash:`ip addr`

- `Reference <https://www.tecmint.com/nmtui-configure-network-connection/>`__

Setup SSH
---------
1. Install ``openssh``: :bash:`sudo pacman -S openssh`
2. Enable service: :bash:`sudo systemctl enable sshd.service`
3. Start service: :bash:`sudo systemctl start sshd.service`
4. Check that service is running: :bash:`systemctl status sshd.service`

Set Up SSH Keys
---------------

- If you already have RSA key pair you can just send public key to remote:
  ::

    cat ~/.ssh/id_rsa.pub | ssh USERNAME@REMOTE_HOST "mkdir -p ~/.ssh && \
      touch ~/.ssh/authorized_keys && chmod -R go= ~/.ssh && cat >> ~/.ssh/authorized_keys"

5. Generate private and public RSA key pair: :bash:`ssh-keygen -b 4096`
6. Select name that is not the same as default if you want to use multiple/different keys.
7. Create also a password
8. Key pair should now be generated, copy it to ``~/.ssh/`` folder
9. Run this command to copy public RSA key to client/remote host:
   :bash:`cat ~/.ssh/RSA_KEY.pub | ssh USERNAME@REMOTE_HOST "mkdir -p ~/.ssh && touch ~/.ssh/authorized_keys && chmod -R go= ~/.ssh && cat >> ~/.ssh/authorized_keys"`
10. You can now login to remote host with :bash:`ssh USERNAME@REMOTE_HOST -i ~/.ssh/RSA_KEY`


- Note that if you use the default RSA key there is no need to pass ``-i RSA_KEY``
- `Reference <https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys-on-ubuntu-20-04>`__

Disabling Password Authentication on remote host
------------------------------------------------
- Edit :bash:`sudo vim /etc/ssh/sshd_config`
- Uncomment and set to ``no``:
  ::

    /etc/ssh/sshd_config
    -----------------------------------------------
    PermitRootLogin no
    PasswordAuthentication no

- `Reference <https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys-on-ubuntu-20-04>`__

Install and enable crontab
--------------------------
1. Install ``crontab``: :bash:`sudo pacman -S cronie`
2. Enable service: :bash:`sudo systemctl enable cronie.service`
3. Start service: :bash:`sudo systemctl start cronie.service`
4. Check that service is running: :bash:`systemctl status cronie.service`

- https://wiki.archlinux.org/index.php/cron[wiki.archlinux.org - cron]


References
==========

- `video - Arch Linux Installation Guide 2020 <https://www.youtube.com/watch?v=PQgyW10xD8s>`__
- `arcolinuxd.com - 5 THE ACTUAL INSTALLATION OF ARCH LINUX PHASE 1 BIOS <https://arcolinuxd.com/5-the-actual-installation-of-arch-linux-phase-1-bios/>`__
- `wiki.archlinux.org- Installation guide <https://wiki.archlinux.org/index.php/Installation_guide>`__


Wifi setup
==========

References
----------
- `wiki.archlinux.org - iwctl <https://wiki.archlinux.org/index.php/Iwd#iwctl>`__
- `wiki.archlinux.org - Wireless <https://wiki.archlinux.org/index.php/Network_configuration/Wireless>`__
- `wiki.archlinux.org - Wireless#Rfkill_caveat <https://wiki.archlinux.org/index.php/Network_configuration/Wireless#Rfkill_caveat>`__
