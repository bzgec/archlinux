# Standard libraries
import traceback

# 3rd party packages
from Xlib import display as xdisplay  # Setup multiple screens dynamically
from libqtile import qtile
from libqtile.log_utils import logger
from libqtile.config import Screen
from libqtile import bar, hook
from libqtile.command import lazy

# Local source
from dflts import WIDGETS_SIZE
from widgets import get_widgets_list

reconfigure_screens = True

# Main screen
screens = [
    Screen(top=bar.Bar(get_widgets_list(isMain=True),
                       size=WIDGETS_SIZE,
                       opacity=1.0)),
]

def get_main_screen():
    return Screen(top=bar.Bar(get_widgets_list(isMain=True),
                              size=WIDGETS_SIZE,
                              opacity=1.0)),

def get_num_monitors():
    num_monitors = 0
    try:
        display = xdisplay.Display()
        screen = display.screen()
        resources = screen.root.xrandr_get_screen_resources()

        for output in resources.outputs:
            monitor = display.xrandr_get_output_info(output, resources.config_timestamp)
            preferred = False
            if hasattr(monitor, "preferred"):
                preferred = monitor.preferred
            elif hasattr(monitor, "num_preferred"):
                preferred = monitor.num_preferred
            if preferred:
                num_monitors += 1
    except Exception as e:
        # always setup at least one monitor
        logger.warning('Exception in get_num_monitors()')
        traceback.print_exception(e)
        return 1
    else:
        return num_monitors

def setup_other_screens():
    global screens

    num_monitors = get_num_monitors()
    logger.warning("Num Monitors is " + str(num_monitors))

    # Other screens
    if num_monitors > 1:
        for m in range(num_monitors - 1):
            screens.append(Screen(top=bar.Bar(get_widgets_list(isMain=False),
                                              size=WIDGETS_SIZE,
                                              opacity=1.0)))

    #logger.warning("Screen change - restart qtile")

setup_other_screens()

@hook.subscribe.screens_reconfigured
def setup_screens_change():
    # Restart qtile
    logger.warning("Screen change - change")
    qtile.cmd_reload_config()
