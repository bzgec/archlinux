# Standard libraries

# 3rd party packages
from libqtile.command import lazy
from libqtile.config import Key, KeyChord

# Local source
from dflts import MOD_KEY, HOME, DFLT_TERM, DFLT_BROWSER, DFLT_TEXT_EDITOR, DFLT_FILE_MANAGER
from helper import change_keyboard_layout, theme_set_light, theme_set_dark, ext_mon_brightness_inc, ext_mon_brightness_dec, ext_mon_brightness_max, ext_mon_brightness_min


# Keys docs: https://docs.qtile.org/en/latest/manual/config/keys.html
# List of keys https://github.com/qtile/qtile/blob/master/libqtile/backend/x11/xkeysyms.py
keys = [
    # Applications
    Key([MOD_KEY], 'Return',
        lazy.spawn(DFLT_TERM),
        desc='Default terminal'),
    Key([MOD_KEY, 'shift'], 'Return',
        lazy.spawn('rofi -show drun -show-icons'),
        desc='Run Launcher'),
    Key([MOD_KEY], 'b',
        lazy.spawn(DFLT_BROWSER),
        desc='Default browser'),
    Key([MOD_KEY], 'e',
        lazy.spawn(DFLT_FILE_MANAGER),
        desc='Default file manager'),
    Key([MOD_KEY], 'c',
        lazy.spawn(DFLT_TEXT_EDITOR),
        desc='Default text editor'),
    Key([MOD_KEY, 'shift'], 's',
        lazy.spawn('flameshot gui'),
        desc='Launch screenshot software'),
    Key([MOD_KEY], 'p',
        lazy.spawn('keepassxc'),
        desc='Password manager'),

    # Custom scripts
    ## Theme selection
    Key([MOD_KEY, 'shift'], 'Home',
        lazy.function(theme_set_light),
        desc='Switch to light theme'),
    Key([MOD_KEY, 'shift'], 'End',
        lazy.function(theme_set_dark),
        desc='Switch to dark theme'),

    ## Change keyboard layout
    Key([MOD_KEY, 'shift'], 'w',
        lazy.function(change_keyboard_layout),
        desc='Change keyboard layout'),

    ## Toggle microphone state
    # TODO: add widget
    Key([MOD_KEY, 'shift'], 'm',
        lazy.spawn('~/.config/scripts/mic/mic toggle -n'),
        desc='Toggle microphone'),
    Key([], 'XF86AudioMicMute',
        lazy.spawn('~/.config/scripts/mic/mic toggle -n'),
        desc='Toggle microphone'),

    ## Monitor selection
    # awful.key({ modkey, "Control" }, "m", function() xrandr.xrandr() end,
    #           {description = "Select monitor setup", group = "Hotkeys"}
    # ),

    #awful.key({ modkey, }, 'd',
    #            function ()
    #                gears.timer {
    #                    timeout   = 0.1,
    #                    single_shot = true,
    #                    autostart = true,
    #                    callback  = function()
    #                        awful.util.spawn('xset dpms force off')
    #                    end
    #                }
    #            end,
    #          {description = 'Bightness control - turn off backlight (untill any key is pressed)', group = 'Hotkeys'}
    #        ),

    # -- X screen locker
    # awful.key({ altkey, "Control" }, "l", function () awful.spawn.with_shell(scrlocker) end,
    #           {description = "lock screen", group = "Hotkeys"}),

    # Music/volume control
    #   - increase - PgUp
    #   - decrease - PgDn
    #   - mute     - End
    #   - unmute   - Home
    # https://wiki.archlinux.org/title/MPRIS
    Key([MOD_KEY], 'Page_Up',
        lazy.spawn('pactl set-sink-volume @DEFAULT_SINK@ +1%'),
        desc='Volume control - increase (pactl)'),
    Key([MOD_KEY], 'Page_Down',
        lazy.spawn('pactl set-sink-volume @DEFAULT_SINK@ -1%'),
        desc='Volume control - decrease (pactl)'),
    Key([MOD_KEY], 'Home',
        lazy.spawn('pactl set-sink-mute @DEFAULT_SINK@ 0'),
        desc='Volume control - unmute (pactl)'),
    Key([MOD_KEY], 'End',
        lazy.spawn('pactl set-sink-mute @DEFAULT_SINK@ 1'),
        desc='Volume control - mute (pactl)'),
    Key([], 'XF86AudioLowerVolume',
        lazy.spawn('pactl set-sink-volume @DEFAULT_SINK@ -1%')),
    Key([], 'XF86AudioRaiseVolume',
        lazy.spawn('pactl set-sink-volume @DEFAULT_SINK@ +1%')),
    Key([], 'XF86AudioMute',
        lazy.spawn('pactl set-sink-mute @DEFAULT_SINK@ toggle')),
    Key([], 'XF86AudioNext',
        lazy.spawn('playerctl next')),
    Key([], 'XF86AudioPrev',
        lazy.spawn('playerctl previous')),
    Key([], 'XF86AudioStop',
        lazy.spawn('playerctl stop')),
    # Always toggle because
    #   - headphones send play and pause
    #   - keyboards send only play (but button is play/pause)
    #Key([], 'XF86AudioPlay',
    #    lazy.spawn('playerctl play-pause')),
    #Key([], 'XF86AudioPause',
    #    lazy.spawn('playerctl play-pause')),
    Key([], 'XF86AudioPlay',
        lazy.spawn('playerctl play')),
    Key([], 'XF86AudioPause',
        lazy.spawn('playerctl pause')),

    # Brightness control:
    #   - increase - PgUp
    #   - decrease - PgDn
    #   - min      - End
    #   - max      - Home
    Key([MOD_KEY, 'control'], 'Page_Up',
        lazy.spawn('xbacklight -inc 5'),
        desc='Bightness control - increase (xbacklight)'),
    Key([MOD_KEY, 'control'], 'Page_Down',
        lazy.spawn('xbacklight -dec 5'),
        desc='Bightness control - decrease (xbacklight)'),
    Key([MOD_KEY, 'control'], 'Home',
        lazy.spawn('xbacklight -set 100'),
        desc='Bightness control - maximal (xbacklight)'),
    Key([MOD_KEY, 'control'], 'End',
        lazy.spawn('xbacklight -set 0'),
        desc='Bightness control - minimal (xbacklight)'),
    Key([], 'XF86MonBrightnessUp',
        lazy.spawn('xbacklight -inc 5')),
    Key([], 'XF86MonBrightnessDown',
        lazy.spawn('xbacklight -dec 5')),
    KeyChord([MOD_KEY], "q",
        [
            KeyChord([], "b",
                [
                    KeyChord([], "e",
                        [
                            Key([], "i", lazy.function(ext_mon_brightness_inc)),
                            Key([], "d", lazy.function(ext_mon_brightness_dec)),
                            Key([], "m", lazy.function(ext_mon_brightness_max)),
                            Key([], "n", lazy.function(ext_mon_brightness_min)),
                        ],
                        name="ext",
                        mode=True),
                ],
                name="brightness",
                mode=True),
        ],
        name="KeyChord",
        mode=True),

    ## Emulate mouse clicks
    #Key([], 'XF86ScrollUp',
    #    #lazy.spawn('xdotool click 4'),
    #    lazy.spawn('xbacklight -inc 5'),
    #    desc='Emulate Button 4 click (scroll up)'),
    #Key([], 'XF86ScrollDown',
    #    #lazy.spawn('xdotool click 5'),
    #    lazy.spawn('xbacklight -dec 5'),
    #    desc='Emulate Button 5 click (scroll down)'),

    # Qtile
    Key([MOD_KEY, 'shift'], 'r',
        lazy.restart(),
        desc='Restart Qtile'),
    #Key([MOD_KEY, 'shift'], 'q',
    #    lazy.shutdown(),
    #    desc='Shutdown Qtile'),
    Key([MOD_KEY, 'shift'], 'c',
        lazy.window.kill(),
        desc='Kill active window'),
    # TODO: show help (keyboard shortcuts)

    # Layouts
    Key([MOD_KEY], 'Tab',
        lazy.next_layout(),
        desc='Toggle through layouts'),
    Key([MOD_KEY, 'shift'], 'Tab',
        lazy.prev_layout(),
        desc='Toggle through layouts (reverse order)'),

    ## Switch focus to specific monitor (out of three)
    #Key([MOD_KEY], 'q',
    #    lazy.to_screen(0),
    #    desc='Keyboard focus to monitor 1'),
    #Key([MOD_KEY], 'w',
    #    lazy.to_screen(1),
    #    desc='Keyboard focus to monitor 2'),
    #Key([MOD_KEY], 'f',
    #    lazy.to_screen(2),
    #    desc='Keyboard focus to monitor 3'),

    ## Switch focus of monitors
    Key([MOD_KEY], 'period',
        lazy.next_screen(),
        desc='Move focus to next monitor'),
    Key([MOD_KEY], 'comma',
        lazy.prev_screen(),
        desc='Move focus to prev monitor'),

    ## Treetab controls
    Key([MOD_KEY, 'shift'], 'h',
        lazy.layout.move_left(),
        desc='Move up a section in treetab'),
    Key([MOD_KEY, 'shift'], 'l',
        lazy.layout.move_right(),
        desc='Move down a section in treetab'),

    ## Window controls
    Key([MOD_KEY], 'j',
        lazy.layout.down(),
        desc='Move focus down in current stack pane'),
    Key([MOD_KEY], 'k',
        lazy.layout.up(),
        desc='Move focus up in current stack pane'),
    Key([MOD_KEY, 'shift'], 'j',
        lazy.layout.shuffle_down(),
        lazy.layout.section_down(),
        desc='Move windows down in current stack'),
    Key([MOD_KEY, 'shift'], 'k',
        lazy.layout.shuffle_up(),
        lazy.layout.section_up(),
        desc='Move windows up in current stack'),
    Key([MOD_KEY], 'h',
        lazy.layout.shrink(),
        lazy.layout.decrease_nmaster(),
        desc='Shrink window (MonadTall), decrease number in master pane (Tile)'),
    Key([MOD_KEY], 'l',
        lazy.layout.grow(),
        lazy.layout.increase_nmaster(),
        desc='Expand window (MonadTall), increase number in master pane (Tile)'),
    Key([MOD_KEY], 'n',
        lazy.layout.normalize(),
        desc='Normalize window size ratios'),
    #Key([MOD_KEY], 'm',
    #    lazy.layout.maximize(),
    #    desc='Toggle window between minimum and maximum sizes'),
    Key([MOD_KEY], 'f',
        lazy.window.toggle_floating(),
        desc='Toggle floating'),
    Key([MOD_KEY], 'm',
        lazy.window.toggle_fullscreen(),
        desc='Toggle fullscreen'),

    ## Stack controls
    #Key([MOD_KEY, 'shift'], 'Tab',
    #    lazy.layout.rotate(),
    #    lazy.layout.flip(),
    #    desc='Switch which side main pane occupies (XmonadTall)'),
    #Key([MOD_KEY], 'space',
    #    lazy.layout.next(),
    #    desc='Switch window focus to other pane(s) of stack'),
    #Key([MOD_KEY, 'shift'], 'space',
    #    lazy.layout.toggle_split(),
    #    desc='Toggle between split and unsplit sides of stack'),
]
