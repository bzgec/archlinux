# Standard libraries
import os
# 3rd party packages

# Local source

# Sets mod key to SUPER/WINDOWS
MOD_KEY = 'mod4'

HOME = os.path.expanduser('~')

FOLDER_ICONS = HOME + '/.config/qtile/icons'

DFLT_TERM = 'alacritty'      # My terminal of choice
DFLT_BROWSER = 'librewolf'   # My browser of choice
DFLT_FILE_MANAGER = 'thunar'  # Default file manager
DFLT_TEXT_EDITOR = 'emacsclient -c -a emacs'  # Default text editor
DFLT_INTERNET_INTERFACE = 'wlp5s0'
DFLT_FONT = 'Ubuntu Mono'
DFLT_FONT_BOLD = 'Ubuntu Mono Bold'
DFLT_FONT_SIZE = 14
DFLT_ICON_SIZE = 20
WIDGETS_SIZE = 22


COLORS_QTILE = {
    "bg_normal": ['#282c34', '#282c34'],  # Normal background
    "bg_focus": ['#1c1f24', '#1c1f24'],  # Focus background
    "fg_normal": ['#dfdfdf', '#dfdfdf'],  # Normal foreground
    "fg_focus": ['#ff6c6b', '#ff6c6b'],  # Focus foreground
    "bg_arrow_0": ['#51afef', '#51afef'],  # Arrow 0
    #"bg_arrow_1": ['#46d9ff', '#46d9ff'],  # Arrow 1
    #"bg_arrow_2": ['#a9a1e1', '#a9a1e1'],  # Arrow 2
    "bg_arrow_1": ['#7197E7', '#7197E7'],  # Arrow 1
    "bg_arrow_2": ['#A77AC4', '#A77AC4'],  # Arrow 2
    "rand_0": ['#98be65', '#98be65'],
    "rand_1": ['#da8548', '#da8548'],
    "rand_2": ['#c678dd', '#c678dd'],
}

# AwesomeWM colors
COLORS_AWESOMEWM = {
    "bg_normal": ['#808080', '#808080'],  # Normal background
    "bg_focus": ['#666666', '#666666'],  # Focus background
    "fg_normal": ['#dcdcef', '#dcdcef'],  # Normal foreground
    "fg_focus": ['#dcdcef', '#dcdcef'],  # Focus foreground
    "bg_arrow_0": ['#51afef', '#51afef'],  # Arrow 0
    "bg_arrow_1": ['#7197E7', '#7197E7'],  # Arrow 1
    "bg_arrow_2": ['#A77AC4', '#A77AC4'],  # Arrow 2
    "rand_0": ['#98be65', '#98be65'],
    "rand_1": ['#51afef', '#51afef'],
    "rand_3": ['#da8548', '#da8548'],
    "rand_2": ['#c678dd', '#c678dd'],
}

#COLORS = COLORS_QTILE
COLORS = COLORS_AWESOMEWM
