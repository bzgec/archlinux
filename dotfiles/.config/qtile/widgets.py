# Standard libraries
import os

# 3rd party packages
from libqtile import qtile
from libqtile import widget
from libqtile.command import lazy

# Local source
from dflts import FOLDER_ICONS, DFLT_TERM, DFLT_INTERNET_INTERFACE, DFLT_FONT, DFLT_FONT_BOLD, DFLT_FONT_SIZE, DFLT_ICON_SIZE, COLORS

ARROW_FONT_SIZE = 37
ARROW_FONT = DFLT_FONT_BOLD

# Default widget settings
widget_defaults = dict(font=DFLT_FONT,
                       fontsize=DFLT_FONT_SIZE,
                       padding=2,
                       background=COLORS['bg_normal'],
                       foreground=COLORS['fg_normal'])

extension_defaults = widget_defaults.copy()

def get_widgets_list(isMain: bool = False) -> list:
    '''Get widgets list (main and other)'''
    widgets_list = []

    # Windows/groups stuff
    widgets_groups_tasks = [
        widget.GroupBox(foreground=COLORS['fg_normal'],
                        font=DFLT_FONT_BOLD,
                        fontsize=DFLT_FONT_SIZE - 2,
                        margin_y=3,
                        margin_x=0,
                        padding_y=5,
                        padding_x=3,
                        borderwidth=3,
                        active=COLORS['fg_normal'],
                        inactive=COLORS['fg_normal'],
                        rounded=False,
                        highlight_color=COLORS['bg_focus'],
                        #highlight_method='line'),
                        highlight_method='line',
                        this_current_screen_border=COLORS['rand_1'],
                        this_screen_border=COLORS['rand_0'],
                        other_current_screen_border=COLORS['rand_1'],
                        other_screen_border=COLORS['rand_0']),
        widget.Sep(),
        widget.CurrentLayoutIcon(foreground=COLORS['fg_normal'],
                                 padding=0,
                                 custom_icon_paths=[FOLDER_ICONS],
                                 scale=0.7),
        #widget.CurrentLayout(foreground=COLORS['fg_normal'],
        #                     padding=5),
        widget.Sep(),
        widget.WindowCount(foreground=COLORS['fg_normal'],
                           padding=5,
                           fmt='{}'),
        widget.Sep(),
        widget.Sep(foreground=COLORS['bg_normal']),
        #widget.WindowName(foreground=COLORS['bg_arrow_0'],
        #                  padding=0),
        widget.TaskList(border=COLORS['fg_normal'],
                        unfocused_border=COLORS['bg_normal'],
                        padding=3,
                        margin=0,
                        spacing=5,
                        icon_size=DFLT_ICON_SIZE - 6,
                        #highlight_method='block',
                        highlight_method='border',
                        max_title_width=200,
                        #title_width_method='uniform',
                        rounded=False,
                        mouse_callbacks={"Button2": lazy.window.kill()},
                        borderwidth=1),
        widget.Chord(background=COLORS['bg_normal'],
                     foreground=COLORS['fg_normal'],
                     padding=5,
                     chords_colors={'vim mode': ('2980b9', 'ffffff')},
                     fmt='Chords: {}',
                     max_chars=10,
                     ),
    ]

    # There can be only one sys tray
    widgets_sys_tray_main = [
        widget.TextBox(foreground=COLORS['bg_arrow_0'],
                       padding=0,
                       text='',
                       fontsize=ARROW_FONT_SIZE),
        widget.StatusNotifier(background=COLORS['bg_arrow_0'],
                              padding=3,
                              icon_size=DFLT_ICON_SIZE),
        widget.Systray(background=COLORS['bg_arrow_0'],
                       padding=3,
                       icon_size=DFLT_ICON_SIZE),
        widget.TextBox(background=COLORS['bg_arrow_0'],
                       foreground=COLORS['bg_arrow_1'],
                       padding=0,
                       text='',
                       fontsize=ARROW_FONT_SIZE),
    ]

    widgets_sys_tray_othr = [
        widget.TextBox(foreground=COLORS['bg_arrow_1'],
                       padding=0,
                       text='',
                       fontsize=ARROW_FONT_SIZE),
    ]

    # Info stuff
    widgets_info = [
        widget.Net(background=COLORS['bg_arrow_1'],
                   foreground=COLORS['fg_normal'],
                   padding=5,
                   interface=DFLT_INTERNET_INTERFACE,
                   format='Net: {down} ↓↑ {up}',
                   font=ARROW_FONT,
                   prefix='k'),
        widget.TextBox(background=COLORS['bg_arrow_1'],
                       foreground=COLORS['bg_arrow_2'],
                       text='',
                       padding=0,
                       fontsize=ARROW_FONT_SIZE),
        widget.Image(background=COLORS['bg_arrow_2'],
                     filename=FOLDER_ICONS + '/cpu.png'),
        widget.CPU(background=COLORS['bg_arrow_2'],
                   foreground=COLORS['fg_normal'],
                   padding=5,
                   format='{load_percent}%',
                   font=ARROW_FONT,
                   update_interval=5),
        widget.ThermalSensor(background=COLORS['bg_arrow_2'],
                             foreground=COLORS['fg_normal'],
                             padding=5,
                             fmt='|   {}',
                             font=ARROW_FONT,
                             tag_sensor='edge',  # AMD CPU
                             update_interval=10,
                             threshold=90),
        #widget.CheckUpdates(
        #         update_interval=1800,
        #         distro='Arch_checkupdates',
        #         display_format='Updates: {updates} ',
        #         foreground=COLORS['fg_normal'],
        #         colour_have_updates=COLORS['bg_focus'],
        #         colour_no_updates=COLORS['bg_focus'],
        #         mouse_callbacks={'Button1': lambda: qtile.cmd_spawn(DFLT_TERM + ' -e sudo pacman -Syu')},
        #         padding=5,
        #         background=COLORS['rand_1']
        #         ),
        widget.TextBox(background=COLORS['bg_arrow_2'],
                       foreground=COLORS['bg_arrow_1'],
                       padding=0,
                       text='',
                       fontsize=ARROW_FONT_SIZE),
        widget.Image(background=COLORS['bg_arrow_1'],
                     filename=FOLDER_ICONS + '/mem.png'),
        widget.Memory(background=COLORS['bg_arrow_1'],
                      foreground=COLORS['fg_normal'],
                      padding=5,
                      measure_mem='G',
                      mouse_callbacks={'Button1': lazy.spawn(DFLT_TERM + ' --class terminal_floating -e htop')},
                      update_interval=10,
                      font=ARROW_FONT,
                      format='{MemUsed: 0.1f} {mm}B'),
        #widget.TextBox(background=COLORS['bg_arrow_0'],
        #               foreground=COLORS['bg_arrow_0'],
        #               padding=0,
        #               text='',
        #               font=DFLT_FONT),
        #widget.KeyboardLayout(background=COLORS['bg_arrow_0'],
        #                      foreground=COLORS['fg_normal'],
        #                      padding=5,
        #                      fmt='Keyboard: {}'),
        widget.TextBox(background=COLORS['bg_arrow_1'],
                       foreground=COLORS['bg_arrow_2'],
                       padding=0,
                       text='',
                       fontsize=ARROW_FONT_SIZE),
        widget.BatteryIcon(background=COLORS['bg_arrow_2'],
                           padding=5,
                           update_interval=10),
        widget.Battery(background=COLORS['bg_arrow_2'],
                       foreground=COLORS['fg_normal'],
                       padding=5,
                       format='Batt: {char} | {percent:2.0%} | {hour:d}:{min:02d} | {watt:.1f} W',
                       font=ARROW_FONT,
                       update_interval=10),
        widget.TextBox(background=COLORS['bg_arrow_2'],
                       foreground=COLORS['bg_arrow_1'],
                       padding=0,
                       text='',
                       fontsize=ARROW_FONT_SIZE),
        widget.Wlan(background=COLORS['bg_arrow_1'],
                    foreground=COLORS['fg_normal'],
                    padding=5,
                    interface=DFLT_INTERNET_INTERFACE,
                    update_interval=10,
                    font=ARROW_FONT,
                    mouse_callbacks={'Button1': lazy.spawn(DFLT_TERM + ' --class terminal_floating -e nmtui')},
                    format='{essid}  |  {percent:2.0%}'),
        widget.TextBox(background=COLORS['bg_arrow_1'],
                       foreground=COLORS['bg_arrow_2'],
                       padding=0,
                       text='',
                       fontsize=ARROW_FONT_SIZE),
        widget.OpenWeather(background=COLORS['bg_arrow_2'],
                           foreground=COLORS['fg_normal'],
                           font=ARROW_FONT,
                           padding=5,
                           format='{location_city}: {temp:.1f}°{units_temperature}',
                           update_interval=600,
                           location='Ljubljana',
                           app_key='2b7e7acd0b69526b00dec2188e55c446'),
        widget.TextBox(background=COLORS['bg_arrow_2'],
                       foreground=COLORS['bg_arrow_1'],
                       padding=0,
                       text='',
                       fontsize=ARROW_FONT_SIZE),
        widget.Volume(background=COLORS['bg_arrow_1'],
                      foreground=COLORS['fg_normal'],
                      padding=5,
                      update_interval=0.05,
                      step=1,
                      font=ARROW_FONT,
                      mouse_callbacks={'Button1': lazy.spawn('pavucontrol')},
                      fmt='Vol: {}'),
        widget.TextBox(background=COLORS['bg_arrow_1'],
                       foreground=COLORS['bg_arrow_2'],
                       padding=0,
                       text='',
                       fontsize=ARROW_FONT_SIZE),
        widget.Clock(background=COLORS['bg_arrow_2'],
                     foreground=COLORS['fg_normal'],
                     format=' %H:%M:%S  %d/%m/%Y ',
                     font=ARROW_FONT,
                     fontsize=DFLT_FONT_SIZE + 1),
    ]

    widgets_list = widgets_list + widgets_groups_tasks

    if isMain is True:
        widgets_list = widgets_list + widgets_sys_tray_main
    else:
        widgets_list = widgets_list + widgets_sys_tray_othr

    widgets_list = widgets_list + widgets_info

    return widgets_list
