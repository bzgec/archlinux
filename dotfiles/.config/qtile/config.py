#!/usr/bin/env python3
'''
Logs file: `~/.local/share/qtile/qtile.log`
Restart Qtile: `qtile cmd-obj -o cmd -f restart`
'''

# Standard libraries
import os
import socket
import subprocess

# 3rd party packages
from libqtile import qtile
from libqtile.config import Click, Drag, Group, Match, Rule, Key
from libqtile.command import lazy
from libqtile import layout, hook

# Local source
from dflts import MOD_KEY, HOME, DFLT_FONT
from keys import keys
from screens import screens, reconfigure_screens
from widgets import widget_defaults, extension_defaults

groups = [
    Group('1', layout='max', persist=True, matches=[Match(wm_class=['librewolf'])]),
    Group('2', layout='monadtall', matches=[Match(wm_class=['emacs', 'vscodium', 'kicad'])]),
    Group('3', layout='monadtall'),
    Group('4', layout='monadtall'),
    Group('5', layout='monadtall', matches=[Match(wm_class=['Steam', 'qbittorrent', 'keepassxc'])]),
    Group('6', layout='monadtall'),
    Group('7', layout='monadtall'),
    Group('8', layout='monadtall'),
    Group('9', layout='monadtall'),
]

dgroups_app_rules = [
    # Floating
    Rule(Match(wm_class=['pavucontrol',
                         'terminal_floating',
                         'snappergui',
                         'qalculate-gtk'],
               ),
         float=True),
]

# Allow MODKEY+[0 through 9] to bind to groups, see https://docs.qtile.org/en/stable/manual/config/groups.html
# MOD4 + index Number : Switch to Group[index]
# MOD4 + shift + index Number : Send active window to another Group
#from libqtile.dgroups import simple_key_binder
#dgroups_key_binder = simple_key_binder(MOD_KEY)

# For some reason above code doesn't work after KeyChord sequence
for i in groups:
    keys.append(Key([MOD_KEY], i.name, lazy.group[i.name].toscreen()))
    keys.append(Key([MOD_KEY, 'shift'], i.name, lazy.window.togroup(i.name)))

layout_theme = {
    'border_width': 2,
    'margin': 1,
    'border_focus': 'e1acff',
    'border_normal': '1D2330'
}

layouts = [
    #layout.MonadWide(**layout_theme),
    #layout.Bsp(**layout_theme),
    #layout.Stack(stacks=2, **layout_theme),
    #layout.Columns(**layout_theme),
    #layout.RatioTile(**layout_theme),
    #layout.Tile(shift_windows=True, **layout_theme),
    #layout.VerticalTile(**layout_theme),
    #layout.Matrix(**layout_theme),
    #layout.Zoomy(**layout_theme),
    layout.MonadTall(**layout_theme),
    layout.Max(**layout_theme),
    layout.Stack(num_stacks=2),
    layout.RatioTile(**layout_theme),
    layout.TreeTab(font=DFLT_FONT,
                   fontsize=10,
                   sections=['FIRST', 'SECOND', 'THIRD', 'FOURTH'],
                   section_fontsize=10,
                   border_width=2,
                   bg_color='1c1f24',
                   active_bg='c678dd',
                   active_fg='000000',
                   inactive_bg='a9a1e1',
                   inactive_fg='1c1f24',
                   padding_left=0,
                   padding_x=0,
                   padding_y=1,
                   section_top=10,
                   section_bottom=20,
                   level_shift=8,
                   vspace=3,
                   panel_width=200),
    layout.Floating(**layout_theme)
]

prompt = '{0}@{1}: '.format(os.environ['USER'], socket.gethostname())

def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)

def window_to_previous_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group)

def window_to_next_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group)

def switch_screens(qtile):
    i = qtile.screens.index(qtile.current_screen)
    group = qtile.screens[i - 1].group
    qtile.current_screen.set_group(group)

mouse = [
    Drag([MOD_KEY], 'Button1', lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([MOD_KEY], 'Button3', lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([MOD_KEY], 'Button2', lazy.window.bring_to_front())
]

# https://docs.qtile.org/en/latest/manual/config/
follow_mouse_focus = False
bring_front_click = True
cursor_warp = True

auto_fullscreen = True
focus_on_window_activation = 'smart'

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True


@hook.subscribe.startup
def startup():
    '''Start applications from autostart.sh
    - emitted every time Qtile starts (including restarts)'''
    #autostart_script = os.path.expanduser('~/.config/qtile/autostart.sh')
    #subprocess.run(autostart_script, check=True)
    subprocess.run([HOME + '/.config/scripts/mic/mic', 'toggle', '-n'])
    subprocess.run([HOME + '/.config/scripts/keymap.sh', 'si_colemak'])


@hook.subscribe.startup_once
def startup_once():
    '''Start applications from autostart.sh once
    - only emitted on the very first startup'''
    autostart_script = HOME + '/.config/qtile/autostart.sh'
    subprocess.run(autostart_script, check=True)

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = 'LG3D'
