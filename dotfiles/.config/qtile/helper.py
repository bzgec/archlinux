# Standard libraries
import subprocess
import traceback

# 3rd party packages
from libqtile import qtile
from libqtile.log_utils import logger
from libqtile.utils import send_notification

# Local source
from dflts import HOME


def run_cmd(cmd: str, get_output: bool = False) -> str:
    '''Run command and return its output (if requested)'''
    return_str = ''

    try:
        result = subprocess.run(cmd, check=True, shell=True, capture_output=get_output)
        if get_output is True:
            return_str = result.stdout.decode("utf-8")

    except subprocess.CalledProcessError as err:
        traceback_str = traceback.format_exc()
        logger.warning(err.stderr.decode("utf-8"))
        logger.warning(traceback_str)

    return return_str


def change_keyboard_layout(qtile):
    '''Change keyboard layout and send notification with current layout'''
    cmd = HOME + '/.config/scripts/keymap.sh'
    title = 'Keyboard layout change'

    text = run_cmd(cmd, get_output=True)

    send_notification(title, text)


def theme_set_light(qtile):
    '''Set light theme and send notification'''
    cmd = HOME + '/.config/scripts/themeSwitcher.py --light'
    title = 'Theme change'

    text = 'Light'

    run_cmd(cmd)

    send_notification(title, text)


def theme_set_dark(qtile):
    '''Set dark theme and send notification'''
    cmd = HOME + '/.config/scripts/themeSwitcher.py --dark'
    title = 'Theme change'

    text = 'Dark'

    run_cmd(cmd)

    send_notification(title, text)


def ext_mon_brightness_inc(qtile, perc=10):
    '''Increase external monitor brightness'''
    cmd = 'ddcutil setvcp 10 + {:d}'.format(perc)

    send_notification('External monitor brightness', '+ {:d}'.format(perc))
    run_cmd(cmd, get_output=False)


def ext_mon_brightness_dec(qtile, perc=10):
    '''Decrease external monitor brightness'''
    cmd = 'ddcutil setvcp 10 - {:d}'.format(perc)

    send_notification('External monitor brightness', '- {:d}'.format(perc))
    run_cmd(cmd, get_output=False)


def ext_mon_brightness_max(qtile):
    '''Increase external monitor brightness to maximum'''
    cmd = 'ddcutil setvcp 10 100'

    send_notification('External monitor brightness', 'Max')
    run_cmd(cmd, get_output=False)


def ext_mon_brightness_min(qtile):
    '''Decrease external monitor brightness to minimum'''
    cmd = 'ddcutil setvcp 10 0'

    send_notification('External monitor brightness', 'Min')
    run_cmd(cmd, get_output=False)
