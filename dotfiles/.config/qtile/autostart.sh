#!/usr/bin/env bash

# Source `.profile`
# shellcheck disable=SC1090
#source "${HOME}/.profile" &

# Set Colemak keyboard layout - set in config.py
#bash ~/.config/scripts/keymap.sh si_colemak &

redshift-gtk -P &

optimus-manager-qt &

udiskie &

keepassxc &

xsettingsd &

nextcloud &

#festival --tts $HOME/.config/qtile/welcome_msg &
#lxsession &
#picom &
/usr/bin/emacs --daemon &
#conky -c $HOME/.config/conky/doomone-qtile.conkyrc
#volumeicon &
#nm-applet &
