#!/usr/bin/env python

import os
import sys
import getopt


def displayHelp():
    print("Script to change the theme of the Alacritty terminal theme ")
    print("and `git diff` (delta) theme.")
    print("Tested with python 3.9.2")
    print("")
    print("python terminalTHemeSwitcher.py [OPTION]")
    print("")
    print("Options:")
    print(" -h, --help    show this help")
    print(" --dark        set all the themes to the dark mode")
    print(" --light       set all the themes to the light mode")


class themeSwitcherClass:
    class themeConfigParamClass:
        def __init__(self, file, startStr, endStr, themeLight, themeDark, sedCmdQuotes):
            self.file = file  # Configuration file which we change
            self.startStr = startStr  # String which specifies the start of an area which we are changing
            self.endStr = endStr  # String which specifies the end of an area which we are changing
            self.themeLight = themeLight  # String which is used as a light theme (in case of Alacritty it is a file)
            self.themeDark = themeDark  # String which is used as a dark theme (in case of Alacritty it is a file)
            self.sedCmdQuotes = sedCmdQuotes  # Which quotes to use with `sed` command

    class themeCmdParamClass:
        def __init__(self, cmdSetThemeLight, cmdSetThemeDark):
            self.cmdSetThemeLight = cmdSetThemeLight  # Command to set light theme
            self.cmdSetThemeDark = cmdSetThemeDark  # Command to set light theme

    def __init__(self):
        self.theme = "light"  # Set default the theme
        self.notify = False  # Notify with `notify-send` program

        # Git configuration
        gitConfigFile       = "~/.gitconfig"
        gitConfigStartStr   = "    #my-select-theme-start"
        gitConfigEndStr     = "    #my-select-theme-end"
        gitConfigThemeLight = "    features = my-feature-theme-light"
        gitConfigThemeDark  = "    features = my-feature-theme-dark"
        gitSedCmdQuotes     = "\""  # If there is single quote present in the sed pattern
                                         # then use an double quotes, and vice versa
        self.gitConfig = self.themeConfigParamClass(gitConfigFile,
                                                    gitConfigStartStr,
                                                    gitConfigEndStr,
                                                    gitConfigThemeLight,
                                                    gitConfigThemeDark,
                                                    gitSedCmdQuotes)

        # Alacritty configuraiton
        alacrittyConfigFile           = "~/.config/alacritty/alacritty.toml"
        alacrittyConfigStartStr       = "#my-color-theme-start"
        alacrittyConfigEndStr         = "#my-color-theme-end"
        alacrittyConfigThemeFileLight = "~/.config/alacritty/themes/themes/gruvbox_light.toml"
        alacrittyConfigThemeFileDark  = "~/.config/alacritty/themes/themes/gruvbox_dark.toml"
        alacrittySedCmdQuotes         = "\""  # If there is single quote present in the sed pattern
                                                   # then use an double quotes, and vice versa
        self.alacrittyConfig = self.themeConfigParamClass(alacrittyConfigFile,
                                                          alacrittyConfigStartStr,
                                                          alacrittyConfigEndStr,
                                                          alacrittyConfigThemeFileLight,
                                                          alacrittyConfigThemeFileDark,
                                                          alacrittySedCmdQuotes)

        # bat configuraiton
        batConfigFile           = "~/.config/bat/config"
        batConfigStartStr       = "#my-color-theme-start"
        batConfigEndStr         = "#my-color-theme-end"
        batConfigThemeLight = "--theme='gruvbox-light'"
        batConfigThemeDark  = "--theme='gruvbox-dark'"
        batSedCmdQuotes         = "'"  # If there is single quote present in the sed pattern
                                                   # then use an double quotes, and vice versa
        self.batConfig = self.themeConfigParamClass(batConfigFile,
                                                    batConfigStartStr,
                                                    batConfigEndStr,
                                                    batConfigThemeLight,
                                                    batConfigThemeDark,
                                                    batSedCmdQuotes)

        nvimConfigFile       = "~/.config/nvim/plug-config/gruvbox.vim"
        nvimConfigStartStr   = '"my-select-theme-start'
        nvimConfigEndStr     = '"my-select-theme-end'
        nvimConfigThemeLight = "set bg=light"
        nvimConfigThemeDark  = "set bg=dark"
        nvimSedCmdQuotes     = "'"  # If there is single quote present in the sed pattern
                                         # then use an double quotes, and vice versa
        self.nvimConfig = self.themeConfigParamClass(nvimConfigFile,
                                                     nvimConfigStartStr,
                                                     nvimConfigEndStr,
                                                     nvimConfigThemeLight,
                                                     nvimConfigThemeDark,
                                                     nvimSedCmdQuotes)

        emacsCmdSetThemeLight = "emacsclient -a emacs --eval '(set-theme-light)' > /dev/null 2>&1"
        emacsCmdSetThemeDark = "emacsclient -a emacs --eval '(set-theme-dark)' > /dev/null 2>&1"
        self.emacsConfig = self.themeCmdParamClass(emacsCmdSetThemeLight, emacsCmdSetThemeDark)

        # Qt5ct config
        qt5ctConfigFile    = "~/.config/qt5ct/qt5ct.conf"
        qt5ctThemeLightStr = 'Adwaita'
        qt5ctThemeIconLightStr = 'breeze'
        qt5ctThemeDarkStr  = 'Adwaita-Dark'
        qt5ctThemeIconDarkStr = 'breeze-dark'
        qt5ctCmdSetThemeLight = f"sed -i 's/^style=.*/style={qt5ctThemeLightStr}/' {qt5ctConfigFile}"
        qt5ctCmdSetThemeLight = qt5ctCmdSetThemeLight + " && " + \
                                f"sed -i 's/^icon_theme=.*/icon_theme={qt5ctThemeIconLightStr}/' {qt5ctConfigFile}"
        qt5ctCmdSetThemeDark  = f"sed -i 's/^style=.*/style={qt5ctThemeDarkStr}/' {qt5ctConfigFile}"
        qt5ctCmdSetThemeDark  = qt5ctCmdSetThemeDark + " && " + \
                                f"sed -i 's/^icon_theme=.*/icon_theme={qt5ctThemeIconDarkStr}/' {qt5ctConfigFile}"
        self.qt5ctConfig = self.themeCmdParamClass(qt5ctCmdSetThemeLight, qt5ctCmdSetThemeDark)

        # GTK2 config
        gtk2ConfigFile    = "~/.gtkrc-2.0"
        gtk2ThemeLightStr = 'Adwaita'
        gtk2ThemeIconLightStr = 'breeze'
        gtk2ThemeDarkStr  = 'Adwaita-dark'
        gtk2ThemeIconDarkStr = 'breeze-dark'
        gtk2CmdSetThemeLight = f"sed -i 's/^gtk-theme-name=.*/gtk-theme-name=\"{gtk2ThemeLightStr}\"/' {gtk2ConfigFile}"
        gtk2CmdSetThemeLight = gtk2CmdSetThemeLight + " && " + \
                               f"sed -i 's/^gtk-icon-theme-name=.*/gtk-icon-theme-name=\"{gtk2ThemeIconLightStr}\"/' {gtk2ConfigFile}"
        gtk2CmdSetThemeDark  = f"sed -i 's/^gtk-theme-name=.*/gtk-theme-name=\"{gtk2ThemeDarkStr}\"/' {gtk2ConfigFile}"
        gtk2CmdSetThemeDark  = gtk2CmdSetThemeDark + " && " + \
                               f"sed -i 's/^gtk-icon-theme-name=.*/gtk-icon-theme-name=\"{gtk2ThemeIconDarkStr}\"/' {gtk2ConfigFile}"
        self.gtk2Config = self.themeCmdParamClass(gtk2CmdSetThemeLight, gtk2CmdSetThemeDark)

        # GTK3 config
        gtk3ConfigFile    = "~/.config/gtk-3.0/settings.ini"
        gtk3ThemeLightStr = 'Adwaita'
        gtk3ThemeIconLightStr = 'breeze'
        gtk3ThemeDarkStr  = 'Adwaita-dark'
        gtk3ThemeIconDarkStr = 'breeze-dark'
        gtk3CmdSetThemeLight = f"sed -i 's/^gtk-theme-name=.*/gtk-theme-name={gtk3ThemeLightStr}/' {gtk3ConfigFile}"
        gtk3CmdSetThemeLight = gtk3CmdSetThemeLight + " && " + \
                               f"sed -i 's/^gtk-icon-theme-name=.*/gtk-icon-theme-name={gtk3ThemeIconLightStr}/' {gtk3ConfigFile}"
        gtk3CmdSetThemeDark  = f"sed -i 's/^gtk-theme-name=.*/gtk-theme-name={gtk3ThemeDarkStr}/' {gtk3ConfigFile}"
        gtk3CmdSetThemeDark  = gtk3CmdSetThemeDark + " && " + \
                               f"sed -i 's/^gtk-icon-theme-name=.*/gtk-icon-theme-name={gtk3ThemeIconDarkStr}/' {gtk3ConfigFile}"
        self.gtk3Config = self.themeCmdParamClass(gtk3CmdSetThemeLight, gtk3CmdSetThemeDark)

        # GTK4 config
        gtk4ThemeLightStr = 'Adwaita'
        gtk4ThemeDarkStr  = 'Adwaita-dark'
        gtk4CmdSetThemeLight = f"gsettings set org.gnome.desktop.interface gtk-theme {gtk4ThemeLightStr}"
        gtk4CmdSetThemeDark  = f"gsettings set org.gnome.desktop.interface gtk-theme {gtk4ThemeDarkStr}"
        self.gtk4Config = self.themeCmdParamClass(gtk4CmdSetThemeLight, gtk4CmdSetThemeDark)

        # X11 xsettings daemon
        xsettConfigFile    = "~/.config/xsettingsd/xsettingsd.conf"
        xsettThemeLightStr = 'Adwaita'
        xsettThemeIconLightStr = 'breeze'
        xsettThemeDarkStr = 'Adwaita-dark'
        xsettThemeIconDarkStr = 'breeze-dark'
        xsettCmdSetThemeLight = f"sed -i 's|^Net/ThemeName.*|Net/ThemeName \"{xsettThemeLightStr}\"|' {xsettConfigFile}"
        xsettCmdSetThemeLight = xsettCmdSetThemeLight + " && " + \
                                f"sed -i 's|^Net/IconThemeName.*|Net/IconThemeName \"{xsettThemeIconLightStr}\"|' {xsettConfigFile}"
        xsettCmdSetThemeDark  = f"sed -i 's|^Net/ThemeName.*|Net/ThemeName \"{xsettThemeDarkStr}\"|' {xsettConfigFile}"
        xsettCmdSetThemeDark  = xsettCmdSetThemeDark + " && " + \
                                f"sed -i 's|^Net/IconThemeName.*|Net/IconThemeName \"{xsettThemeIconDarkStr}\"|' {xsettConfigFile}"
        self.xsettConfig = self.themeCmdParamClass(xsettCmdSetThemeLight, xsettCmdSetThemeDark)

# Replace all text between two lines
# `sed -e '/BEGIN/,/END/c\BEGIN\nfine, thanks\nEND' file`
#   - reference: https://stackoverflow.com/a/5179968
def replaceBetweenLines(themeConfigParam, theme):
    if os.path.exists(themeConfigParam.file) is True:
        file = themeConfigParam.file
        startStr = themeConfigParam.startStr
        endStr = themeConfigParam.endStr
        if theme == "light":
            newContext = themeConfigParam.themeLight
        elif theme == "dark":
            newContext = themeConfigParam.themeDark
        else:
            print("Wrong theme selected!")
            sys.exit(3)

        if themeConfigParam.sedCmdQuotes == '"':
            sedCmd = "sed -i \"/" + startStr + "/,/" + endStr + "/c\\" + startStr + "\\n" + newContext + "\\n" + endStr + "\" " + file
        elif themeConfigParam.sedCmdQuotes == "'":
            sedCmd = "sed -i '/" + startStr + "/,/" + endStr + "/c\\" + startStr + "\\n" + newContext + "\\n" + endStr + "' " + file
        else:
            print("Wrong quotes char specified: " + quotesChar)
            sys.exit(4)

        # print("sed command: " + sedCmd)
        os.system(sedCmd)
    else:
        print("File doesn't exists!")
        print("    File: " + file)
        sys.exit(2)

def setThemeAlacritty(themeConfigParam, theme):
    # Here we cheat a little bit because we don't just replace a string in the configuration
    # file but we need to copy a theme file into the configuration file

    if os.path.exists(themeConfigParam.file) and os.path.exists(themeConfigParam.themeLight) and \
            os.path.exists(themeConfigParam.themeDark):

        # Select a file which we need to copy into the configuration file
        if theme == "light":
            alacrittyThemeFile = themeConfigParam.themeLight
        else:
            alacrittyThemeFile = themeConfigParam.themeDark

        # Fix file context so it can be passed into the `sed` command
        with open(alacrittyThemeFile) as f:
            alacrittyThemeFileData = f.read()
            alacrittyThemeFileData = alacrittyThemeFileData.replace("\n", "\\n")
            alacrittyThemeFileData = alacrittyThemeFileData.replace("\r", "\\r")
            alacrittyThemeFileData = alacrittyThemeFileData.replace("\"", "\\\"")

        # Dirty hack in order to use the same `replaceBetweenLines()` function
        # We now use the same `.themeLight` or `.themeDark` variable which previously
        # held a file path.
        # We can do this because we won't need this variables any more
        if theme == "light":
            themeConfigParam.themeLight = alacrittyThemeFileData
        else:
            themeConfigParam.themeDark = alacrittyThemeFileData

        replaceBetweenLines(themeConfigParam, theme)

    else:
        print("Alacritty files don't exist!")
        print("    Alacritty configuration file: " + themeConfigParam.file)
        print("    Alacritty light theme file: " + themeConfigParam.themeLight)
        print("    Alacritty dark theme file: " + themeConfigParam.themeDark)
        sys.exit(2)

def setThemeWithCmd(themeConfigParam, theme):
    if theme == "light":
        os.system(themeConfigParam.cmdSetThemeLight)
    elif theme == "dark":
        os.system(themeConfigParam.cmdSetThemeDark)
    else:
        os.system(themeConfigParam.cmdSetThemeLight)

# X11 xsettings daemon
# (every gtk app will listen to it, if you want to live reload to a different
# theme edit `~/.config/xsettingsd/xsettingsd.conf` and force reload of
# the config with `killall -s HUP xsettingsd`
# https://www.reddit.com/r/archlinux/comments/r34p5i/comment/hmb6sxk/?utm_source=share&utm_medium=web2x&context=3
def reloadXsettingsDaemon():
    os.system('killall -s HUP xsettingsd')

def setThemes(themeSwitcher):
    replaceBetweenLines(themeSwitcher.gitConfig, themeSwitcher.theme)
    setThemeAlacritty(themeSwitcher.alacrittyConfig, themeSwitcher.theme)
    replaceBetweenLines(themeSwitcher.batConfig, themeSwitcher.theme)
    #replaceBetweenLines(themeSwitcher.nvimConfig, themeSwitcher.theme)
    setThemeWithCmd(themeSwitcher.emacsConfig, themeSwitcher.theme)
    setThemeWithCmd(themeSwitcher.qt5ctConfig, themeSwitcher.theme)
    setThemeWithCmd(themeSwitcher.gtk2Config, themeSwitcher.theme)
    setThemeWithCmd(themeSwitcher.gtk3Config, themeSwitcher.theme)
    setThemeWithCmd(themeSwitcher.gtk4Config, themeSwitcher.theme)
    setThemeWithCmd(themeSwitcher.xsettConfig, themeSwitcher.theme)
    reloadXsettingsDaemon()

def replaceTildeWithFullPathInStr(strWithTilde):
    home = os.path.expanduser("~")
    return strWithTilde.replace("~", home)

def replaceTildeWithFullPathSingle(themeConfigParam):
    themeConfigParam.file = replaceTildeWithFullPathInStr(themeConfigParam.file)

    # This is needed in case the `.themeLight` or `.themeDark` is file path instead of just
    # a string (this is with the alacritty)
    themeConfigParam.themeLight = replaceTildeWithFullPathInStr(themeConfigParam.themeLight)
    themeConfigParam.themeDark = replaceTildeWithFullPathInStr(themeConfigParam.themeDark)

def replaceTildeWithFullPathAll(themeSwitcher):
    replaceTildeWithFullPathSingle(themeSwitcher.gitConfig)
    replaceTildeWithFullPathSingle(themeSwitcher.alacrittyConfig)
    replaceTildeWithFullPathSingle(themeSwitcher.batConfig)
    replaceTildeWithFullPathSingle(themeSwitcher.nvimConfig)

def notify(theme: str):
    theme_msg = "Unknown"

    if theme == "light":
        theme_msg = "Light"
    elif theme == "dark":
        theme_msg = "Dark"

    os.system(f'notify-send "Theme: {theme_msg}"')

# https://www.tutorialspoint.com/python/python_command_line_arguments.htm
def checkArgs(themeSwitcher, argv):
    try:
        opts, args = getopt.getopt(argv, "h", ["help", "light", "dark", "notify"])
    except getopt.GetoptError:
        displayHelp()
        sys.exit(1)

    for opt, arg in opts:
        if opt in ('-h', "--help"):
            displayHelp()
            sys.exit(0)
        elif opt in ("--light"):
            themeSwitcher.theme = "light"
        elif opt in ("--dark"):
            themeSwitcher.theme = "dark"
        elif opt in ("--notify"):
            themeSwitcher.notify = True

if __name__ == "__main__":

    themeSwitcher = themeSwitcherClass()
    checkArgs(themeSwitcher, sys.argv[1:])

    replaceTildeWithFullPathAll(themeSwitcher)

    if themeSwitcher.notify is True:
        notify(themeSwitcher.theme)

    if themeSwitcher.theme == "light" or themeSwitcher.theme == "dark":
        setThemes(themeSwitcher)
    else:
        print("Wrong theme selected!")
        sys.exit(3)

    sys.exit(0)

