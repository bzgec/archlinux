#!/bin/bash

set -euo pipefail  # https://gist.github.com/maxisam/e39efe89455d26b75999418bf60cf56c
# set -x  # Print all executed commands

# Set the path to your lock screen image
SCRIPT_LOCATION="$HOME/.config/scripts/screen-locker"
IMAGE_PATH="$SCRIPT_LOCATION/lock-screen-background.jpg"

AUTORANDAR_CONFIG=$(autorandr --current)

LOCK_BG="$SCRIPT_LOCATION/gen-images/screen_$AUTORANDAR_CONFIG.png"

# Check that "gen-images" folder exists
mkdir -p "$SCRIPT_LOCATION/gen-images"

if [ -f "$LOCK_BG" ]; then
    echo "Lock screen image exists"
else
    # Lock screen image does not exist, generate new lock screen image
    echo "Lock screen image does not exist, generate new lock screen image"

    # Get screen information
    SCREENS=$(xrandr --listmonitors | tail -n +2 | awk '{print $3}')
    WIDTH=$(xrandr | grep 'Screen 0' | awk '{print $8}')
    HEIGHT=$(xrandr | grep 'Screen 0' | awk '{print $10}')
    RESOLUTION="${WIDTH}x${HEIGHT}"

    # Create a blank background with the same resolution as the combined screen size
    # Force a sRGB colorspace for the image (to avoid grayscale) - prefix image with `PNG32:`
    # (can be checked with `identify -verbose gen-images/screen_.png | grep Colorspace`)
    magick -size "$RESOLUTION" xc:black PNG32:"$LOCK_BG"

    # Overlay the image onto the background for each monitor
    for SCREEN in $SCREENS; do
        SCREEN_WIDTH=$(echo "$SCREEN" | cut -d'/' -f1 | cut -d'x' -f1)
        SCREEN_HEIGHT=$(echo "$SCREEN" | cut -d'/' -f1 | cut -d'x' -f2)
        OFFSET_X=$(echo "$SCREEN" | cut -d'+' -f2)
        OFFSET_Y=$(echo "$SCREEN" | cut -d'+' -f3)

        magick "$LOCK_BG" "$IMAGE_PATH" -geometry "${SCREEN_WIDTH}x${SCREEN_HEIGHT}+$OFFSET_X+$OFFSET_Y" -composite "$LOCK_BG"
    done
fi

# Lock the screen with the generated image
# and check if i3lock succeeded
if ! i3lock -fe -i "$LOCK_BG"; then
    i3lock -fe --color=000000
fi
