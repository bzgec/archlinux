# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1='[\u@\h \W\$ '

export ALTERNATE_EDITOR=""   # setting for emacsclient
export EDITOR="nvim"         # $EDITOR use Emacs in terminal
export VISUAL="nvim"         # $VISUAL use Emacs in GUI mode

# Check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

################################################################################
# Bash history management
################################################################################
# https://gist.github.com/marioBonales/1637696
#
# Don't store commands started with space and don't save new command if previous was the same
# ignoreboth == ignorespace and ignoredups
HISTCONTROL=ignoreboth

# Append to the history file, don't overwrite it
shopt -s histappend

# HISTSIZE: The number of commands to remember in the command history (list)
HISTSIZE=10000
# HISTFILESIZE: The  maximum  number of lines contained in the history file
HISTFILESIZE=10000

# PROMPT_COMMAND: contents of this variable is run as regular command before bash show prompt.
#                 So every time after you execute a command contents of this variable is also
#                 executed.
# 'history -a': append history lines from this session to the history file
# 'history -n': read all history lines not already read from the history file
#               and append them to the history list
# 'history -r': read the history file and append the contents to the history list
# export PROMPT_COMMAND="history -a; history -n"
export PROMPT_COMMAND="history -a"

alias bashsynchistorylist="history -r"
################################################################################

################################################################################
# fzf
################################################################################
# Add key-bindings: 'Ctrl+R', 'Ctrl+T', 'Alt+C'
# https://github.com/junegunn/fzf?tab=readme-ov-file#key-bindings-for-command-line
source /usr/share/fzf/key-bindings.bash

# https://github.com/junegunn/fzf?tab=readme-ov-file#fuzzy-completion-for-bash-and-zsh
source /usr/share/fzf/completion.bash

# Type
# kill -9 **
# then press TAB
export FZF_COMPLETION_TRIGGER='**'
# export FZF_DEFAULT_COMMAND=

alias fzfp='fzf --preview "bat --style=numbers --color=always {}"'
alias fzfnoignore="rg --no-ignore --hidden -g '!*.git' --files | fzf"  # `rg` DOESN'T respects .gitignore...
alias fzfnoignorep="rg --no-ignore --hidden -g '!*.git' --files | fzfp"  # `rg` DOESN'T respects .gitignore...
alias fzfignore="rg --hidden -g '!*.git' --files | fzf"  # `rg` respects .gitignore...
alias fzfignorep="rg --hidden -g '!*.git' --files | fzfp"  # `rg` respects .gitignore...
################################################################################

################################################################################
# Starship
################################################################################
##################################################
# Terminal title
##################################################
# Set termTitle to empty string - if set it is used as terminal title
termTitle=''

# Set tab title funtion
# https://makandracards.com/makandra/21369-bash-setting-the-title-of-your-terminal-tab
# https://unix.stackexchange.com/questions/216953/show-only-current-and-parent-directory-in-bash-prompt
function tabTitleSet {
  # Check if termTitle var is set
  if [[ ! "$termTitle" ]]; then
    # termTitle var not set, check if we are trying to set it
    if [ -z "$1" ]; then
      # Set tab title as current directory
      # title=${PWD##*/}  # Current directory
      # title=${PWD}  # Current directory - full path
      title=$(basename $(dirname "$PWD"))/$(basename "$PWD")  # Current dir and parent dirr
    else
      # set termTitle var
      termTitle=$1  # First param
      title=$1  # first param
    fi
  else
    title=$termTitle
  fi

  echo -ne "\033]0;$title\007"
}

function tabTitleReset {
  termTitle=''
}

# Set cb function when command is executed in terminal
# https://starship.rs/advanced-config/#change-window-title
starship_precmd_user_func="tabTitleSet"

##################################################
# Enable starship prompt
##################################################
eval "$(starship init bash)"
################################################################################


################################################################################
# HSTR configuration - add this to ~/.bashrc
################################################################################
# alias hh=hstr                    # hh to be alias for hstr
# export HSTR_CONFIG=hicolor       # get more colors
# shopt -s histappend              # append new history items to .bash_history
# export HISTCONTROL=ignoreboth    # leading space hides commands from history
# export HISTFILESIZE=10000        # increase history file size (default is 500)
# export HISTSIZE=${HISTFILESIZE}  # increase history size (default is 500)
# # ensure synchronization between bash memory and history file
# export PROMPT_COMMAND="history -a; history -n; ${PROMPT_COMMAND}"
# function hstrnotiocsti {
#     { READLINE_LINE="$( { </dev/tty hstr ${READLINE_LINE}; } 2>&1 1>&3 3>&- )"; } 3>&1;
#     READLINE_POINT=${#READLINE_LINE}
# }
# # if this is interactive shell, then bind hstr to Ctrl-r (for Vi mode check doc)
# if [[ $- =~ .*i.* ]]; then bind -x '"\C-r": "hstrnotiocsti"'; fi
# export HSTR_TIOCSTI=n

################################################################################

# Set nvim as a MANPAGER
#export MANPAGER="nvim -c 'set ft=man' -"

# Set Alacritty as TERM
# export TERM=alacritty

################################################################################
# Change path
################################################################################
# Custom gcc path
export PATH="$PATH":~/gcc-arm-none-eabi/bin

# Python pip scripts
export PATH="$PATH":~/.local/bin

# Tmux
export PATH="$PATH":~/.config/tmux

# Doom Emacs
export PATH="$PATH":~/.emacs.d/bin

# Go
export PATH="$PATH":~/go/bin

# AppImage-s
export PATH="$PATH":~/apps

# Gem (ceedling)
export PATH="$PATH":~/.local/share/gem/ruby/3.0.0/bin
################################################################################

################################################################################
# Doom Emacs
################################################################################
alias cemacs="emacsclient -c -a 'emacs'"
alias doomsync="~/.config/emacs/bin/doom sync"
alias doomdoctor="~/.config/emacs/bin/doom doctor"
alias doomupgrade="~/.config/emacs/bin/doom upgrade"
alias doompurge="~/.config/emacs/bin/doom purge"
################################################################################

rm_ssh_key () {
  ssh-keygen -f "~/.ssh/known_hosts" -R "192.168.64.2"
}

nucleo_uart_115200() {
  minicom -D /dev/ttyACM0 -b 115200
}

gitlog() {
  git log --pretty=format:'%C(yellow)%h %Cred%ad %C(cyan)%an%C(auto)%d %C(reset)%s' --date=format:'%Y/%m/%d %H:%M:%S' --all --graph
}
alias gits="git status -s"

killproc() {
    ps aux | fzf | awk '{print $2}' | xargs kill
}

killproc9() {
    ps aux | fzf | awk '{print $2}' | xargs -I{} kill -9 {}
}

copyfolder() {
    command pwd | xclip -r -selection clipboard
}

copyfilename() {
    command ls | fzf | xclip -r -selection clipboard
}

copygithash() {
    git log --pretty=format:'%C(yellow)%h %Cred%ad %C(cyan)%an%C(auto)%d %C(reset)%s' --date=format:'%Y/%m/%d %H:%M:%S' --all | fzf | awk '{print $1}' | xclip -r -selection clipboard
}

checkserial() {
    if ls /dev/ttyUSB* 1> /dev/null 2>&1; then
        ls /dev/ttyUSB*
    else
        echo "No /dev/ttyUSB* devices"
    fi

    if ls /dev/ttyACM* 1> /dev/null 2>&1; then
        ls /dev/ttyACM*
    else
        echo "No /dev/ttyACM* devices"
    fi
}

copyserial() {
    if ls /dev/ttyUSB* 1> /dev/null 2>&1 || ls /dev/ttyACM* 1> /dev/null 2>&1; then
        ls /dev/tty* | grep -E '/dev/ttyUSB|/dev/ttyACM' | fzf | xclip -r -selection clipboard
    fi
}

alias ls='ls --color=auto'
alias ll='ls -halF'
alias la='ls -hA'
alias l='ls -hCF'

alias light="~/.config/scripts/themeSwitcher.py --light"
alias dark="~/.config/scripts/themeSwitcher.py --dark"

alias updateMirrorlist="sudo reflector --latest 20 --sort rate --save /etc/pacman.d/mirrorlist"

alias keymap="~/.config/scripts/keymap.sh"
alias reload-wm="leftwm-command SoftReload"

alias tmuxs="tmux-sessionizer"

alias get_idf='. $HOME/esp/esp-idf/export.sh'
alias espidf_setup='. $HOME/esp/esp-idf/export.sh'

alias vpn-conn="wg-quick up wg0"
alias vpn-dis="wg-quick down wg0"

alias autodisplay="autorandr -c"
alias mobile="autorandr -l mobile"
alias docked-lj="autorandr -l docked_lj"
alias docked-lj2="autorandr -l docked_lj2"
alias docked-pe="autorandr -l docked_pe"
alias docked-pe2="autorandr -l docked_pe_samsung_big"

alias get-ip="curl https://icanhazip.com/"

# First parameter is password length
gen_pass () {
  openssl rand -base64 $1
}

# First parameter is password length
gen_pass_simp () {
  openssl rand -base64 $1 | tr -dc '[:alnum:]\n\r'
}

# $1 is for additional flags (line `-p`)
corne_build_l () {
    west build $1 -d build/left -b nice_nano_v2 -- -DSHIELD=corne_left -DZMK_CONFIG="/home/bzgec/projects/keyboard/zmk-config-corne/config"
}

# $1 is for additional flags (line `-p`)
corne_build_r () {
    west build $1 -d build/right -b nice_nano_v2 -- -DSHIELD=corne_right -DZMK_CONFIG="/home/bzgec/projects/keyboard/zmk-config-corne/config"
}

# $1 is for additional flags (line `-p`)
corne_build () {
    corne_build_l $1
    corne_build_r $1
}

corne_flash_l() {
    cp ~/projects/keyboard/zmk/app/build/left/zephyr/zmk.uf2 /run/media/bzgec/NICENANO/
}

corne_flash_r() {
    cp ~/projects/keyboard/zmk/app/build/right/zephyr/zmk.uf2 /run/media/bzgec/NICENANO/
}

# source /home/bzgec/.bash_completions/typer.sh
# source /home/bzgec/.bash_completions/make.py.sh
export DOOMWADDIR="$HOME/.config/gzdoom"

